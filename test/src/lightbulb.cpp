#include "wiring/lightbulb.hpp"

#include "catch.hpp"

TEST_CASE("Lightbulb Constructor", "[lightbulb][init][unit]")
{
	LightBulb lightbulb;
	REQUIRE(lightbulb.getInputSize() == 1);
	REQUIRE(lightbulb.getOutputSize() == 0);
	REQUIRE(lightbulb.getOutput() == Signal(Signal::State::UNDEFINED));
}

TEST_CASE("Lightbulb Update", "[lightbulb][update][unit]")
{
	LightBulb lightbulb;

	lightbulb.setInput(0, Signal::State::ERROR);
	REQUIRE(lightbulb.getOutput() == Signal(Signal::State::ERROR));

	lightbulb.setInput(0, Signal::State::FALSE);
	REQUIRE(lightbulb.getOutput() == Signal(Signal::State::FALSE));

	lightbulb.setInput(0, Signal::State::TRUE);
	REQUIRE(lightbulb.getOutput() == Signal(Signal::State::TRUE));
}

TEST_CASE("Lightbulb Resize Inputs", "[lightbulb][resize][unit]")
{
	LightBulb lightbulb;
	REQUIRE_THROWS(lightbulb.resizeInputs(2));
}

TEST_CASE("Lightbulb Resize Outputs", "[lightbulb][resize][unit]")
{
	LightBulb lightbulb;
	REQUIRE_THROWS(lightbulb.resizeOutputs(1));
}
