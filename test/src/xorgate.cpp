#include "gate/xorgate.hpp"

#include "catch.hpp"

TEST_CASE("Xor Gate Constructor", "[xorgate][init][unit]")
{

	SECTION("Default", "Make a 2 input gate")
	{
		XorGate gate;
		REQUIRE(gate.getInputSize() == 2);
		REQUIRE(gate.getOutputSize() == 1);
	}

	SECTION("Given input size", "Make a 5 input gate")
	{
		XorGate gate = XorGate(5);
		REQUIRE(gate.getInputSize() == 5);
		REQUIRE(gate.getOutputSize() == 1);
	}
}

TEST_CASE("Xor Gate Update", "[xorgate][update][unit]")
{
	XorGate gate;

	REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

	SECTION("Input error", "One input is error")
	{
		gate.setInput(1, Signal::State::ERROR);

		gate.setInput(0, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);

		gate.setInput(0, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);

		gate.setInput(0, Signal::State::UNDEFINED);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);
	}

	SECTION("Input undefined", "One input is undefined")
	{
		gate.setInput(1, Signal::State::UNDEFINED);

		gate.setInput(0, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

		gate.setInput(0, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

		gate.setInput(0, Signal::State::UNDEFINED);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);
	}

	SECTION("Positive output", "Output should be true")
	{
		gate.setInput(0, Signal::State::TRUE);
		gate.setInput(1, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);

		gate.setInput(0, Signal::State::FALSE);
		gate.setInput(1, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);
	}

	SECTION("Negative output", "Output should be false")
	{
		gate.setInput(0, Signal::State::FALSE);
		gate.setInput(1, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);

		gate.setInput(0, Signal::State::TRUE);
		gate.setInput(1, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);
	}
}

TEST_CASE("Xor Gate Resize Inputs", "[xorgate][resize][unit]")
{
	XorGate gate;
	gate.setInput(0, Signal::State::FALSE);
	gate.setInput(1, Signal::State::TRUE);

	gate.resizeInputs(4);
	REQUIRE(gate.getInputSize() == 4);
	REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

	gate.resizeInputs(2);
	REQUIRE(gate.getInputSize() == 2);
	REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);

	REQUIRE_THROWS(gate.resizeInputs(1));
}

TEST_CASE("Xor Gate Resize Outputs", "[xorgate][resize][unit]")
{
	XorGate gate;
	REQUIRE_THROWS(gate.resizeOutputs(2));
}
