#include "gate/notgate.hpp"

#include "catch.hpp"

TEST_CASE("Not Gate Constructor", "[notgate][init][unit]")
{
	NotGate gate;
	REQUIRE(gate.getInputSize() == 1);
	REQUIRE(gate.getOutputSize() == 1);
}

TEST_CASE("Not Gate Update", "[notgate][update][unit]")
{
	NotGate gate;

	REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);

	SECTION("Input error", "Input is error")
	{
		gate.setInput(0, Signal::State::ERROR);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::ERROR);
	}

	SECTION("Input undefined", "Input is undefined")
	{
		gate.setInput(0, Signal::State::UNDEFINED);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::UNDEFINED);
	}

	SECTION("Positive output", "Output should be true")
	{
		gate.setInput(0, Signal::State::FALSE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::TRUE);
	}

	SECTION("Negative output", "Output should be false")
	{
		gate.setInput(0, Signal::State::TRUE);
		gate.update();
		REQUIRE(gate.getOutput(0).getState() == Signal::State::FALSE);
	}
}

TEST_CASE("Not Gate Resize Inputs", "[notgate][resize][unit]")
{
	NotGate gate;
	REQUIRE_THROWS(gate.resizeInputs(2));
}

TEST_CASE("Not Gate Resize Outputs", "[notgate][resize][unit]")
{
	NotGate gate;
	REQUIRE_THROWS(gate.resizeOutputs(2));
}
