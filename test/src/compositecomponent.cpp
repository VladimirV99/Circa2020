#include "component/compositecomponent.hpp"

#include "catch.hpp"
#include "circuitboard.hpp"
#include "component/componentfactory.hpp"
#include "connection/connectionmanager.hpp"
#include "wiring/lightbulb.hpp"
#include "wiring/pin.hpp"
#include "wiring/wire.hpp"

TEST_CASE("Composite Simple", "[composite][integration]")
{
	CircuitBoard board;

	Pin *pin = ComponentFactory::instance().makePin();
	Wire *w1 = ComponentFactory::instance().makeWire();
	NotGate *gate = ComponentFactory::instance().makeNotGate();
	Wire *w2 = ComponentFactory::instance().makeWire();
	LightBulb *lightbulb = ComponentFactory::instance().makeLightBulb();

	board.add(pin);
	board.add(w1);
	board.add(gate);
	board.add(w2);
	board.add(lightbulb);

	ConnectionManager::Instance().connect(pin, 0, w1, 0);
	ConnectionManager::Instance().connect(w1, 1, gate, 0);
	ConnectionManager::Instance().connect(gate, 0, w2, 0);
	ConnectionManager::Instance().connect(w2, 1, lightbulb, 0);

	CompositeComponent *composite = ComponentFactory::instance().makeComposite({pin}, {lightbulb});
	board.add(composite);

	REQUIRE(composite->getInputSize() == 1);
	REQUIRE(composite->getOutputSize() == 1);
	REQUIRE(composite->getInput(0) == Signal(Signal::State::UNDEFINED));

	SECTION("Update", "Calculate the output")
	{
		Pin *input = ComponentFactory::instance().makePin();
		LightBulb *output = ComponentFactory::instance().makeLightBulb();

		ConnectionManager::Instance().connect(input, 0, composite, 0);
		ConnectionManager::Instance().connect(composite, 0, output, 0);

		REQUIRE(output->getOutput() == Signal(Signal::State::UNDEFINED));

		input->setState(Signal::State::ERROR);
		REQUIRE(output->getOutput() == Signal(Signal::State::ERROR));

		input->setState(Signal::State::FALSE);
		REQUIRE(output->getOutput() == Signal(Signal::State::TRUE));

		input->setState(Signal::State::TRUE);
		REQUIRE(output->getOutput() == Signal(Signal::State::FALSE));
	}

	//    SECTION("Resize inputs", "Throw an error when resizing") {
	//        REQUIRE_THROWS(composite->resizeInputs(2));
	//    }

	//    SECTION("Resize outputs", "Throw an error when resizing") {
	//        REQUIRE_THROWS(composite->resizeOutputs(2));
	//    }
}

TEST_CASE("Composite Adder", "[composite][integration]")
{
	CircuitBoard board;

	Pin *pin1 = ComponentFactory::instance().makePin();
	Pin *pin2 = ComponentFactory::instance().makePin();
	Wire *w1 = ComponentFactory::instance().makeWire();
	Wire *w2 = ComponentFactory::instance().makeWire();
	Wire *w3 = ComponentFactory::instance().makeWire();
	Wire *w4 = ComponentFactory::instance().makeWire();
	Wire *w5 = ComponentFactory::instance().makeWire();
	Wire *w6 = ComponentFactory::instance().makeWire();
	XorGate *gateSum = ComponentFactory::instance().makeXorGate();
	AndGate *gateCarry = ComponentFactory::instance().makeAndGate();
	LightBulb *lbSum = ComponentFactory::instance().makeLightBulb();
	LightBulb *lbCarry = ComponentFactory::instance().makeLightBulb();

	board.add(pin1);
	board.add(pin2);
	board.add(w1);
	board.add(w2);
	board.add(w3);
	board.add(w4);
	board.add(w5);
	board.add(w6);
	board.add(gateSum);
	board.add(gateCarry);
	board.add(lbSum);
	board.add(lbCarry);

	ConnectionManager::Instance().connect(pin1, 0, w1, 0);
	ConnectionManager::Instance().connect(pin2, 0, w2, 0);

	ConnectionManager::Instance().connect(w1, 1, w3, 0);
	ConnectionManager::Instance().connect(w3, 0, w1, 1);
	ConnectionManager::Instance().connect(w1, 2, w4, 0);
	ConnectionManager::Instance().connect(w4, 0, w1, 2);

	ConnectionManager::Instance().connect(w2, 1, w5, 0);
	ConnectionManager::Instance().connect(w5, 0, w2, 1);
	ConnectionManager::Instance().connect(w2, 2, w6, 0);
	ConnectionManager::Instance().connect(w6, 0, w2, 2);

	ConnectionManager::Instance().connect(w3, 1, gateSum, 0);
	ConnectionManager::Instance().connect(w5, 1, gateSum, 1);

	ConnectionManager::Instance().connect(w4, 1, gateCarry, 0);
	ConnectionManager::Instance().connect(w6, 1, gateCarry, 1);

	ConnectionManager::Instance().connect(gateSum, 0, lbSum, 0);
	ConnectionManager::Instance().connect(gateCarry, 0, lbCarry, 0);

	CompositeComponent *composite = ComponentFactory::instance().makeComposite({pin1, pin2}, {lbSum, lbCarry});
	board.add(composite);

	REQUIRE(composite->getInputSize() == 2);
	REQUIRE(composite->getOutputSize() == 2);
	REQUIRE(composite->getInput(0) == Signal(Signal::State::UNDEFINED));
	REQUIRE(composite->getInput(1) == Signal(Signal::State::UNDEFINED));

	SECTION("Update", "Calculate the output")
	{
		Pin *input1 = ComponentFactory::instance().makePin(Signal::State::FALSE);
		Pin *input2 = ComponentFactory::instance().makePin(Signal::State::FALSE);
		LightBulb *outputSum = ComponentFactory::instance().makeLightBulb();
		LightBulb *outputCarry = ComponentFactory::instance().makeLightBulb();

		ConnectionManager::Instance().connect(input1, 0, composite, 0);
		ConnectionManager::Instance().connect(input2, 0, composite, 1);
		ConnectionManager::Instance().connect(composite, 0, outputSum, 0);
		ConnectionManager::Instance().connect(composite, 1, outputCarry, 0);

		REQUIRE(outputSum->getOutput() == Signal(Signal::State::FALSE));
		REQUIRE(outputCarry->getOutput() == Signal(Signal::State::FALSE));

		input1->flip();
		REQUIRE(outputSum->getOutput() == Signal(Signal::State::TRUE));
		REQUIRE(outputCarry->getOutput() == Signal(Signal::State::FALSE));

		input2->flip();
		REQUIRE(outputSum->getOutput() == Signal(Signal::State::FALSE));
		REQUIRE(outputCarry->getOutput() == Signal(Signal::State::TRUE));

		input1->flip();
		REQUIRE(outputSum->getOutput() == Signal(Signal::State::TRUE));
		REQUIRE(outputCarry->getOutput() == Signal(Signal::State::FALSE));
	}

	//    SECTION("Resize inputs", "Throw an error when resizing") {
	//        REQUIRE_THROWS(composite->resizeInputs(4));
	//    }

	//    SECTION("Resize outputs", "Throw an error when resizing") {
	//        REQUIRE_THROWS(composite->resizeOutputs(4));
	//    }
}

TEST_CASE("Composite Nested", "[composite][integration]")
{
	CircuitBoard board;

	Pin *pinNested = ComponentFactory::instance().makePin();
	Wire *wireNested = ComponentFactory::instance().makeWire();
	LightBulb *lightbulbNested = ComponentFactory::instance().makeLightBulb();

	board.add(pinNested);
	board.add(wireNested);
	board.add(lightbulbNested);

	ConnectionManager::Instance().connect(pinNested, 0, wireNested, 0);
	ConnectionManager::Instance().connect(wireNested, 1, lightbulbNested, 0);

	CompositeComponent *nested = ComponentFactory::instance().makeComposite({pinNested}, {lightbulbNested});
	board.add(nested);

	Pin *pin = ComponentFactory::instance().makePin();
	LightBulb *lightbulb = ComponentFactory::instance().makeLightBulb();

	board.add(pin);
	board.add(lightbulb);

	ConnectionManager::Instance().connect(pin, 0, nested, 0);
	ConnectionManager::Instance().connect(nested, 0, lightbulb, 0);

	CompositeComponent *composite = ComponentFactory::instance().makeComposite({pin}, {lightbulb});
	board.add(composite);

	Pin *input = ComponentFactory::instance().makePin();
	LightBulb *output = ComponentFactory::instance().makeLightBulb();

	ConnectionManager::Instance().connect(input, 0, composite, 0);
	ConnectionManager::Instance().connect(composite, 0, output, 0);

	REQUIRE(output->getOutput() == Signal(Signal::State::UNDEFINED));

	input->setState(Signal::State::ERROR);
	REQUIRE(output->getOutput() == Signal(Signal::State::ERROR));

	input->setState(Signal::State::FALSE);
	REQUIRE(output->getOutput() == Signal(Signal::State::FALSE));

	input->setState(Signal::State::TRUE);
	REQUIRE(output->getOutput() == Signal(Signal::State::TRUE));
}
