set(SRC_PATH "${TEST_PATH}/src")

Include(FetchContent)

FetchContent_Declare(
  Catch2
  GIT_REPOSITORY https://github.com/catchorg/Catch2.git
  GIT_TAG        v2.13.3)

FetchContent_MakeAvailable(Catch2)

list(APPEND CMAKE_MODULE_PATH ${catch2_SOURCE_DIR}/contrib)

set(SOURCES
    ${SRC_PATH}/buffer.cpp
    ${SRC_PATH}/constant.cpp
    ${SRC_PATH}/main.cpp
    ${SRC_PATH}/andgate.cpp
    ${SRC_PATH}/nandgate.cpp
    ${SRC_PATH}/norgate.cpp
    ${SRC_PATH}/notgate.cpp
    ${SRC_PATH}/orgate.cpp
    ${SRC_PATH}/xorgate.cpp
    ${SRC_PATH}/xnorgate.cpp
    ${SRC_PATH}/pin.cpp
    ${SRC_PATH}/lightbulb.cpp
    ${SRC_PATH}/wire.cpp
    ${SRC_PATH}/compositecomponent.cpp
)

set(TEST_NAME ${PROJECT_NAME}_test)
add_executable(${TEST_NAME} ${SOURCES})
target_include_directories(${TEST_NAME} PRIVATE
    "${catch2_SOURCE_DIR}/single_include/catch2"
)
target_link_libraries(${TEST_NAME} PRIVATE
    ${LIB_NAME}
    Catch2::Catch2
)

enable_testing()
include(CTest)
include(Catch)
catch_discover_tests(${TEST_NAME})
