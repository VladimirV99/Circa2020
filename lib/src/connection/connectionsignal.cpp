#include "connectionsignal.hpp"

#include <map>

Signal::Signal(Signal::State value) : m_state(value)
{
}

Signal &Signal::operator=(const Signal &other)
{
	m_state = other.getState();
	return *this;
}

Signal::State Signal::getState() const
{
	return this->m_state;
}

bool Signal::operator==(const Signal &s) const
{
	return this->m_state == s.m_state;
}

bool Signal::operator!=(const Signal &s) const
{
	return this->m_state != s.m_state;
}

Signal Signal::operator!() const
{
	if (m_state == Signal::State::FALSE)
		return Signal(Signal::State::TRUE);

	if (m_state == Signal::State::TRUE)
		return Signal(Signal::State::FALSE);

	return Signal(m_state);
}

std::ostream &operator<<(std::ostream &os, const Signal &s)
{
	static std::map<Signal::State, std::string> Strings;
	if (Strings.empty()) {
		Strings.insert(std::pair<Signal::State, std::string>(Signal::State::ERROR, "Error"));
		Strings.insert(std::pair<Signal::State, std::string>(Signal::State::UNDEFINED, "Undefined"));
		Strings.insert(std::pair<Signal::State, std::string>(Signal::State::TRUE, "True"));
		Strings.insert(std::pair<Signal::State, std::string>(Signal::State::FALSE, "False"));
	}

	return os << Strings[s.m_state];
}

int Signal::toInt(Signal::State state)
{
	switch (state) {
	case State::UNDEFINED:
		return 0;
	case State::ERROR:
		return 1;
	case State::FALSE:
		return 2;
	case State::TRUE:
		return 3;
	}
	throw "Unknown signal";
}

Signal::State Signal::fromInt(int s)
{
	switch (s) {
	case 0:
		return State::UNDEFINED;
	case 1:
		return State::ERROR;
	case 2:
		return State::FALSE;
	case 3:
		return State::TRUE;
	}
	throw "Unknown signal";
}

void Signal::serialize(std::ostream &os) const
{
	int s = toInt(m_state);
	os.write((char *)(&s), sizeof(s));
}

void Signal::deserialize(std::istream &is)
{
	int s;
	is.read((char *)(&s), sizeof(s));
	m_state = fromInt(s);
}
