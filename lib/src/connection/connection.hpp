#ifndef CONNECTION_H
#define CONNECTION_H

#include <map>

#include "connectionsignal.hpp"

class Component;

class Connection
{
public:
	Connection();
	Connection(const Connection &other);
	Connection &operator=(const Connection &other);

	operator bool() const;

	bool hasChanged() const;
	void flush();
	void reset();

	void setSignal(const Signal &s);
	const Signal &getSignal() const;

	void serialize(std::ostream &os) const;
	void deserialize(std::istream &is, std::map<int, Component *> cb);

private:
	void connect(Component *to, unsigned index);
	void disconnect();

public:
	bool connected;
	Component *to_component;
	unsigned to_index;

private:
	Signal signal;
	bool changed;

	friend class ConnectionManager;
};

#endif // CONNECTION_H
