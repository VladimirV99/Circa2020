#include "connection.hpp"

#include "component/component.hpp"

bool Connection::hasChanged() const
{
	return changed;
}

void Connection::flush()
{
	changed = false;
}

void Connection::setSignal(const Signal &s)
{
	if (signal == s)
		return;
	signal = s;
	changed = true;
}

const Signal &Connection::getSignal() const
{
	return signal;
}

Connection::Connection() : connected(false), to_component(nullptr), to_index(0), changed(false)
{
}

Connection::Connection(const Connection &other) = default;

Connection &Connection::operator=(const Connection &other)
{
	connected = other.connected;
	to_component = other.to_component;
	to_index = other.to_index;
	changed = other.changed;
	return *this;
}

Connection::operator bool() const
{
	return connected;
}

void Connection::reset()
{
	connected = false;
	signal = Signal(Signal::State::UNDEFINED);
	changed = true;
}

void Connection::serialize(std::ostream &os) const
{
	os.write((char *)(&connected), sizeof(connected));

	if (connected) {
		int to_component_id = to_component->getId();
		if (to_component_id == -1)
			throw "Serialization error";
		os.write((char *)(&to_component_id), sizeof(to_component_id));
		os.write((char *)(&to_index), sizeof(to_index));
	}

	signal.serialize(os);
	os.write((char *)(&changed), sizeof(changed));
}

void Connection::deserialize(std::istream &is, std::map<int, Component *> cb)
{
	is.read((char *)(&connected), sizeof(connected));

	if (connected) {
		int to_component_id;
		is.read((char *)(&to_component_id), sizeof(to_component_id));
		auto it = cb.find(to_component_id);
		if (it == cb.cend())
			throw "Deserialization error";
		to_component = it->second;
		is.read((char *)(&to_index), sizeof(to_index));
	}

	signal.deserialize(is);
	is.read((char *)(&changed), sizeof(changed));
}

void Connection::connect(Component *to, unsigned index)
{
	connected = true;
	to_component = to;
	to_index = index;
}

void Connection::disconnect()
{
	connected = false;
}
