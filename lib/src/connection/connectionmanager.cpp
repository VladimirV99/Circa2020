#include "connectionmanager.hpp"

#include <unordered_set>

ConnectionManager &ConnectionManager::Instance()
{
	static ConnectionManager Instance;
	return Instance;
}

bool ConnectionManager::connect(Component *output, unsigned output_index, Component *input, unsigned input_index)
{
	auto &output_connection = output->output_connections[output_index];
	auto &input_connection = input->input_connections[input_index];

	if (output_connection || input_connection)
		return false;

	output_connection.connect(input, input_index);
	input_connection.connect(output, output_index);
	if (output->getType() == ComponentTypes::WIRE && input->getType() == ComponentTypes::WIRE) {
		output->input_connections[output_index].connect(input, input_index);
		input->output_connections[input_index].connect(output, output_index);
	}

	input->notifyStateChanged();
	if (output->getType() == ComponentTypes::WIRE && input->getType() == ComponentTypes::WIRE)
		output->notifyStateChanged();
	return true;
}

bool ConnectionManager::connect(
	Component *output, unsigned output_index, Component *input, unsigned input_index, bool direction)
{
	if (direction)
		return connect(output, output_index, input, input_index);
	else
		return connect(input, input_index, output, output_index);
}

bool ConnectionManager::disconnectInput(Component *component, unsigned index)
{
	auto &connection = component->input_connections[index];
	if (!connection)
		return false;
	Component *other = connection.to_component;
	unsigned other_index = connection.to_index;
	connection.disconnect();
	other->output_connections[other_index].disconnect();
	component->input_connections[index].setSignal(Signal::State::UNDEFINED);
	component->notifyStateChanged();
	return true;
}

void ConnectionManager::disconnectInputs(Component *component, unsigned start, unsigned end)
{
	for (unsigned index = start; index < end; index++) {
		auto &connection = component->input_connections[index];
		if (!connection)
			continue;
		Component *other = connection.to_component;
		unsigned other_index = connection.to_index;
		connection.disconnect();
		other->output_connections[other_index].disconnect();
		component->input_connections[index].setSignal(Signal::State::UNDEFINED);
	}
	component->notifyStateChanged();
}

void ConnectionManager::disconnectInputs(Component *component)
{
	for (auto &connection : component->input_connections) {
		if (!connection)
			continue;
		Component *other = connection.to_component;
		unsigned other_index = connection.to_index;
		connection.disconnect();
		other->output_connections[other_index].disconnect();
		connection.setSignal(Signal::State::UNDEFINED);
	}
	component->notifyStateChanged();
}

bool ConnectionManager::disconnectOutput(Component *component, unsigned index)
{
	auto &connection = component->output_connections[index];
	if (!connection)
		return false;
	Component *other = connection.to_component;
	unsigned other_index = connection.to_index;
	connection.disconnect();
	other->input_connections[other_index].disconnect();
	other->input_connections[other_index].setSignal(Signal::State::UNDEFINED);
	other->notifyStateChanged();
	return true;
}

void ConnectionManager::disconnectOutputs(Component *component, unsigned start, unsigned end)
{
	std::unordered_set<Component *> others;
	for (unsigned index = start; index < end; index++) {
		auto &connection = component->output_connections[index];
		if (!connection)
			continue;
		Component *other = connection.to_component;
		unsigned other_index = connection.to_index;
		connection.disconnect();
		other->input_connections[other_index].disconnect();
		other->input_connections[other_index].setSignal(Signal::State::UNDEFINED);
		others.insert(other);
	}
	for (auto *other : others)
		other->notifyStateChanged();
}

void ConnectionManager::disconnectOutputs(Component *component)
{
	std::unordered_set<Component *> others;
	for (auto &connection : component->output_connections) {
		if (!connection)
			continue;
		Component *other = connection.to_component;
		unsigned other_index = connection.to_index;
		connection.disconnect();
		other->input_connections[other_index].disconnect();
		other->input_connections[other_index].setSignal(Signal::State::UNDEFINED);
		others.insert(other);
	}
	for (auto *other : others)
		other->notifyStateChanged();
}

void ConnectionManager::moveInput(Component *component, unsigned index_from, unsigned index_to, bool update)
{
	if (component->input_connections[index_to])
		throw "Connection is already in use";
	component->input_connections[index_to] = component->input_connections[index_from];
	component->input_connections[index_from].reset();
	if (component->input_connections[index_to]) {
		auto &other = component->input_connections[index_to].to_component;
		other->output_connections[component->input_connections[index_to].to_index].to_index = index_to;
	}
	if (update)
		component->notifyStateChanged();
}

void ConnectionManager::transferWireConnection(Component *from, Component *to, unsigned index)
{
	transferInput(from, index, to, index, false);
	transferOutput(from, index, to, index, false);
}

void ConnectionManager::transferInput(
	Component *from_component, unsigned from_index, Component *to_component, unsigned to_index, bool update)
{
	auto &to_connection = to_component->input_connections[to_index];
	auto &from_connection = from_component->input_connections[from_index];
	if (!from_connection)
		return;
	if (to_connection)
		throw "Connection is already in use";
	// Copy connection data but keep signal
	to_connection = from_connection;
	// Update other side of connection
	auto &other = to_connection.to_component->output_connections[to_connection.to_index];
	other.to_component = to_component;
	other.to_index = to_index;
	// Disconnect src
	from_connection.disconnect();
	from_connection.signal = Signal::State::UNDEFINED;
	// Update input sides
	if (update) {
		from_component->notifyStateChanged();
		to_component->notifyStateChanged();
	}
}

void ConnectionManager::transferOutput(
	Component *from_component, unsigned from_index, Component *to_component, unsigned to_index, bool update)
{
	auto &to_connection = to_component->output_connections[to_index];
	auto &from_connection = from_component->output_connections[from_index];
	if (!from_connection)
		return;
	if (to_connection)
		throw "Connection is already in use";
	// Copy connection data but keep signal
	to_connection = from_connection;
	// Update other side of connection
	auto &other = to_connection.to_component->input_connections[to_connection.to_index];
	other.to_component = to_component;
	other.to_index = to_index;
	// Disconnect src
	from_connection.disconnect();
	// Update input side
	if (update)
		to_connection.to_component->notifyStateChanged();
}
