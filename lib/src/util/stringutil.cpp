#include "stringutil.hpp"

#include <iterator>
#include <sstream>

StringUtil::StringUtil() = default;

std::string StringUtil::join(const std::vector<std::string> &v, const char *delim)
{
	std::ostringstream os;
	auto b = begin(v);
	auto e = end(v);

	if (b != e) {
		std::copy(b, prev(e), std::ostream_iterator<std::string>(os, delim));
		b = prev(e);
	}
	if (b != e) {
		os << *b;
	}

	return os.str();
}

std::string StringUtil::parenthesize_if(const std::string &s, bool condition)
{
	if (condition) {
		std::ostringstream os;
		os << "(" << s << ")";
		return os.str();
	} else {
		return s;
	}
}
