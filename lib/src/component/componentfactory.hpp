#ifndef COMPONENTFACTORY_H
#define COMPONENTFACTORY_H

#include <algorithm>
#include <unordered_map>
#include <unordered_set>

#include "arithmetic/adder.hpp"
#include "arithmetic/comparator.hpp"
#include "arithmetic/divider.hpp"
#include "arithmetic/multiplier.hpp"
#include "arithmetic/negator.hpp"
#include "arithmetic/subtractor.hpp"
#include "circuitboard.hpp"
#include "component/compositecomponent.hpp"
#include "componenttype.hpp"
#include "connection/connectionmanager.hpp"
#include "gate/andgate.hpp"
#include "gate/buffer.hpp"
#include "gate/nandgate.hpp"
#include "gate/norgate.hpp"
#include "gate/notgate.hpp"
#include "gate/orgate.hpp"
#include "gate/xnorgate.hpp"
#include "gate/xorgate.hpp"
#include "memory/dflipflop.hpp"
#include "memory/jkflipflop.hpp"
#include "memory/srflipflop.hpp"
#include "memory/tflipflop.hpp"
#include "plexer/decoder.hpp"
#include "plexer/demultiplexer.hpp"
#include "plexer/encoder.hpp"
#include "plexer/multiplexer.hpp"
#include "wiring/clock.hpp"
#include "wiring/constant.hpp"
#include "wiring/lightbulb.hpp"
#include "wiring/pin.hpp"
#include "wiring/splitter.hpp"
#include "wiring/wire.hpp"

using CopyCache = std::unordered_map<Component *, Component *>;
using ComponentSelection = std::unordered_set<Component *>;
using ShouldCopyPredicate = std::function<bool(Component *)>;
using NewCopyCallback = std::function<void(Component *)>;

class ComponentFactory
{
public:
	static ComponentFactory &instance();

	[[nodiscard]] Component *make(ComponentTypes type);
	[[nodiscard]] Pin *makePin(Signal::State s = Signal::State::UNDEFINED);
	[[nodiscard]] LightBulb *makeLightBulb();
	[[nodiscard]] Wire *makeWire();
	[[nodiscard]] Constant *makeConstant(Signal::State);
	[[nodiscard]] Clock *makeClock();
	[[nodiscard]] Buffer *makeBuffer();
	[[nodiscard]] AndGate *makeAndGate();
	[[nodiscard]] OrGate *makeOrGate();
	[[nodiscard]] NotGate *makeNotGate();
	[[nodiscard]] XorGate *makeXorGate();
	[[nodiscard]] NandGate *makeNandGate();
	[[nodiscard]] NorGate *makeNorGate();
	[[nodiscard]] XnorGate *makeXnorGate();
	[[nodiscard]] Multiplexer *makeMultiplexer();
	[[nodiscard]] Demultiplexer *makeDemultiplexer();
	[[nodiscard]] Encoder *makeEncoder();
	[[nodiscard]] Decoder *makeDecoder();
	[[nodiscard]] Comparator *makeComparator();
	[[nodiscard]] Adder *makeAdder();
	[[nodiscard]] Subtractor *makeSubtractor();
	[[nodiscard]] Multiplier *makeMultiplier();
	[[nodiscard]] Divider *makeDivider();
	[[nodiscard]] Negator *makeNegator();
	[[nodiscard]] Splitter *makeSplitter();
	[[nodiscard]] SRFlipFlop *makeSRFlipFlop();
	[[nodiscard]] JKFlipFlop *makeJKFlipFlop();
	[[nodiscard]] TFlipFlop *makeTFlipFlop();
	[[nodiscard]] DFlipFlop *makeDFlipFlop();
	[[nodiscard]] CompositeComponent *makeComposite(std::vector<Pin *>, std::vector<LightBulb *>);

	[[nodiscard]] Component *shallowCopy(Component *);
	Component *deepCopy(Component *component, CircuitBoard *into, CopyCache &cache);

	Component *deepCopy(Component *, CircuitBoard *into, CopyCache &, ShouldCopyPredicate);

	template <typename First, typename Last>
	Component *deepCopySelection(
		Component *comp, CopyCache &cache, First selectionBegin, Last selectionEnd, CircuitBoard *into)
	{
		return deepCopy(comp, into, cache, [&selectionBegin, selectionEnd](auto p) {
			return std::find(selectionBegin, selectionEnd, p) != selectionEnd;
		});
	}

	template <typename First, typename Last>
	CompositeComponent *makeCompositeFromSelection(First selectionBegin, Last selectionEnd)
	{
		std::vector<Pin *> pins;
		std::vector<LightBulb *> lbs;
		std::for_each(selectionBegin, selectionEnd, [&pins, &lbs](Component *component) {
			if (component->getType() == ComponentTypes::PIN)
				pins.push_back(static_cast<Pin *>(component));
			else if (component->getType() == ComponentTypes::LIGHTBULB)
				lbs.push_back(static_cast<LightBulb *>(component));
		});

		auto *cc = new CompositeComponent(pins.size(), lbs.size());
		CopyCache cache;
		for (size_t i = 0; i < pins.size(); i++) {
			auto *p = pins[i];
			cc->setPin(
				i, static_cast<Pin *>(
					   ComponentFactory::deepCopySelection(p, cache, selectionBegin, selectionEnd, cc)));
		}
		for (size_t i = 0; i < lbs.size(); i++) {
			auto *lb = lbs[i];
			cc->setLb(
				i, static_cast<LightBulb *>(
					   ComponentFactory::deepCopySelection(lb, cache, selectionBegin, selectionEnd, cc)));
		}
		return cc;
	}

private:
	ComponentFactory() = default;
	ComponentFactory(const ComponentFactory &other) = delete;
};

#endif // COMPONENTFACTORY_H
