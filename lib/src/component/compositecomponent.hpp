#ifndef COMPOSITECOMPONENT_H
#define COMPOSITECOMPONENT_H

#include <unordered_set>
#include <vector>

#include "circuitboard.hpp"
#include "component.hpp"
#include "wiring/lightbulb.hpp"
#include "wiring/pin.hpp"

/**
 * @brief The CompositeComponent class
 * Should not do any copying or traversal
 * logic itself, it should be done before
 * construction (most likely by a factory)
 */
// TODO make CircuitBoard a member, dont inherit. Currently that would break copying objects to same circuitboard
class CompositeComponent : public Component, public CircuitBoard
{
protected:
	std::vector<Pin *> pins;
	std::vector<LightBulb *> lbs;

public:
	CompositeComponent(unsigned, unsigned);
	void add(Component *c, bool newId = true) override;

	// Not sure if any of these are necessary
	void setPin(unsigned, Pin *);
	void setLb(unsigned, LightBulb *);
	Pin *getPin(unsigned);
	LightBulb *getLb(unsigned);

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is, std::map<int, Component *> lookup) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
	friend class ComponentFactory;
};

#endif // COMPOSITECOMPONENT_H
