#include "compositecomponent.hpp"

#include "componentfactory.hpp"

CompositeComponent::CompositeComponent(unsigned nin, unsigned nout) : Component(nin, nout)
{
}

void CompositeComponent::add(Component *c, bool newId)
{
	// Don't add pins and lbs if deserializing
	if (newId) {
		if (c->getType() == ComponentTypes::PIN) {
			this->pins.push_back(static_cast<Pin *>(c));
		} else if (c->getType() == ComponentTypes::LIGHTBULB) {
			this->lbs.push_back(static_cast<LightBulb *>(c));
		}
	}
	CircuitBoard::add(c, newId);
}

void CompositeComponent::setPin(unsigned int idx, Pin *p)
{
	this->pins[idx] = p;
}
void CompositeComponent::setLb(unsigned int idx, LightBulb *lb)
{
	this->lbs[idx] = lb;
}

Pin *CompositeComponent::getPin(unsigned int idx)
{
	return this->pins[idx];
}

LightBulb *CompositeComponent::getLb(unsigned int idx)
{
	return this->lbs[idx];
}

void CompositeComponent::update()
{
	for (unsigned i = 0; i < this->getInputSize(); i++) {
		// TODO Check for change, not connection
		// The component may have been disconnected and UNDEFINED should be propagated
		if (!this->input_connections[i].connected)
			continue;

		this->pins[i]->setState(this->input_connections[i].getSignal().getState());
	}

	for (unsigned i = 0; i < this->getOutputSize(); i++) {
		this->output_connections[i].setSignal(this->lbs[i]->getOutput());
	}
}

void CompositeComponent::serialize(std::ostream &os) const
{
	Component::serialize(os);
	serializeBoard(os);

	size_t nPins = pins.size();
	os.write((char *)(&nPins), sizeof(nPins));
	for (auto *pin : pins) {
		int id = pin->getId();
		os.write((char *)(&id), sizeof(id));
	}

	size_t nLbs = lbs.size();
	os.write((char *)(&nLbs), sizeof(nLbs));
	for (auto *lb : lbs) {
		int id = lb->getId();
		os.write((char *)(&id), sizeof(id));
	}
}

void CompositeComponent::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	Component::deserialize(is, lookup);

	deserializeBoard(is);

	size_t nPins;
	is.read((char *)(&nPins), sizeof(nPins));
	for (size_t i = 0; i < nPins; i++) {
		int id;
		is.read((char *)(&id), sizeof(id));
		pins.push_back(static_cast<Pin *>(get(id)));
	}

	size_t nLbs;
	is.read((char *)(&nLbs), sizeof(nLbs));
	for (size_t i = 0; i < nLbs; i++) {
		int id;
		is.read((char *)(&id), sizeof(id));
		lbs.push_back(static_cast<LightBulb *>(get(id)));
	}
}

ComponentTypes CompositeComponent::getType() const
{
	return ComponentTypes::COMPOSITE;
}
