#ifndef ENCODER_H
#define ENCODER_H

#include "component/component.hpp"

class Encoder : public Component
{

public:
	Encoder();
	~Encoder() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // ENCODER_H
