#ifndef DEMULTIPLEXER_H
#define DEMULTIPLEXER_H

#include "component/component.hpp"

class Demultiplexer : public Component
{

public:
	Demultiplexer();
	~Demultiplexer() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;

private:
	unsigned m_num_of_select_inputs;
};

#endif // DEMULTIPLEXER_H
