#include "multiplexer.hpp"

#include <cmath>

#include "connection/connectionmanager.hpp"

Multiplexer::Multiplexer() : Gate(3), m_num_of_select_inputs(1)
{
}

Multiplexer::~Multiplexer() = default;

void Multiplexer::resizeInputs(unsigned n)
{

	unsigned inputs = this->getInputSize() - m_num_of_select_inputs;

	if (n == inputs)
		return;

	if (n < inputs) {
		ConnectionManager::Instance().disconnectInputs(this, n, this->getInputSize() - m_num_of_select_inputs);
		m_num_of_select_inputs = std::ceil(std::log2(n));

		for (unsigned i = 0; i < m_num_of_select_inputs; i++) {
			ConnectionManager::Instance().moveInput(this, inputs + i, n + i, false);
		}

		ConnectionManager::Instance().disconnectInputs(this, n + m_num_of_select_inputs, this->getInputSize());

		this->input_connections.resize(n + m_num_of_select_inputs);
		this->notifyStateChanged();
	}

	if (n > inputs) {
		unsigned previous_select_inputs = m_num_of_select_inputs;

		m_num_of_select_inputs = std::ceil(std::log2(n));
		this->input_connections.resize(n + m_num_of_select_inputs);

		for (int i = previous_select_inputs - 1; i >= 0; i--) {
			ConnectionManager::Instance().moveInput(
				this, inputs + i, this->getInputSize() - m_num_of_select_inputs + i, false);
		}

		this->notifyStateChanged();
	}
}

Signal Multiplexer::op()
{

	for (unsigned i = this->getInputSize() - m_num_of_select_inputs; i < this->getInputSize(); i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::ERROR) {
			return Signal::State::ERROR;
		}
	}

	for (unsigned i = this->getInputSize() - m_num_of_select_inputs; i < this->getInputSize(); i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::UNDEFINED) {
			return Signal::State::UNDEFINED;
		}
	}

	unsigned input = 0;
	unsigned exponent = 0;
	for (unsigned i = this->getInputSize() - m_num_of_select_inputs; i < this->getInputSize(); i++) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
			input += std::pow(2, exponent);
		exponent += 1;
	}

	if (input >= this->getInputSize() - m_num_of_select_inputs) {
		return Signal::State::ERROR;
	}

	return this->input_connections[input].getSignal();
}

void Multiplexer::serialize(std::ostream &os) const
{
	Component::serialize(os);
	os.write((char *)(&m_num_of_select_inputs), sizeof(m_num_of_select_inputs));
}

void Multiplexer::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	Component::deserialize(is, lookup);
	is.read((char *)(&m_num_of_select_inputs), sizeof(m_num_of_select_inputs));
}

ComponentTypes Multiplexer::getType() const
{
	return ComponentTypes::MULTIPLEXER;
}
