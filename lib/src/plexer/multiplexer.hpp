#ifndef MULTIPLEXER_H
#define MULTIPLEXER_H

#include "gate/gate.hpp"

class Multiplexer : public Gate
{

public:
	Multiplexer();
	~Multiplexer() override;

	void resizeInputs(unsigned n) override;

	ComponentTypes getType() const override;

	unsigned m_num_of_select_inputs;

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is, std::map<int, Component *> lookup) override;

protected:
	Signal op() override;
};

#endif // MULTIPLEXER_H
