#ifndef ORNODE_H
#define ORNODE_H

#include <string>
#include <vector>

#include "opnode.hpp"

class OrNode : public OpNode
{
public:
	OrNode(std::vector<OpNode *> m_inputs);
	~OrNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	std::vector<OpNode *> getInputs() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	std::vector<OpNode *> m_inputs;
};

#endif // ORNODE_H
