#ifndef LIGHTBULBNODE_H
#define LIGHTBULBNODE_H

#include "opnode.hpp"

class OutputNode : public OpNode
{
public:
	OutputNode(int id, OpNode *input);
	~OutputNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	OpNode *getInput() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	int m_id;
	OpNode *m_input;
};

#endif // LIGHTBULBNODE_H
