#include "andnode.hpp"

#include <algorithm>
#include <sstream>
#include <utility>

#include "constantnode.hpp"
#include "notnode.hpp"
#include "util/stringutil.hpp"

AndNode::AndNode(std::vector<OpNode *> inputs) : m_inputs(std::move(inputs))
{
}

AndNode::~AndNode()
{
	for (auto *node : m_inputs)
		delete node;
}

std::string AndNode::toString() const
{
	std::vector<std::string> input_formulae;
	for (auto *node : m_inputs) {
		input_formulae.push_back(
			StringUtil::parenthesize_if(node->toString(), getPriority(node->getType()) > getPriority(getType())));
	}
	return StringUtil::join(input_formulae, " & ");
}

OpNode *AndNode::simplify()
{
	OpNode *replacement = nullptr;

	std::vector<OpNode *> simplified_inputs;
	for (auto *node : m_inputs) {
		OpNode *simplified = node->simplify();
		// Discard all constants
		if (simplified->getType() == OpType::CONSTANT) {
			auto *n = static_cast<ConstantNode *>(simplified);
			if (!n->getValue()) {
				replacement = new ConstantNode(false);
				break;
			} else {
				delete simplified;
				continue;
			}
		} else if (simplified->getType() == OpType::AND) {
			// join all nested and gates
			auto *n = static_cast<AndNode *>(simplified);
			for (auto *subnode : n->getInputs())
				simplified_inputs.push_back(subnode->simplify());
			delete simplified;
		} else {
			simplified_inputs.push_back(simplified);
		}
	}

	if (replacement != nullptr) {
		for (auto *node : simplified_inputs) {
			delete node;
		}
		return replacement;
	}

	// Remove duplicates
	std::vector<OpNode *> filtered_inputs;
	for (unsigned i = 0; i < simplified_inputs.size(); i++) {
		bool found = false;
		for (unsigned j = 0; j < i; j++) {
			if (equals(simplified_inputs[i], simplified_inputs[j])) {
				found = true;
				break;
			}
		}
		if (!found)
			filtered_inputs.push_back(simplified_inputs[i]);
		else
			delete simplified_inputs[i];
	}

	// check !A & A
	for (unsigned i = 0; i < filtered_inputs.size(); i++) {
		if (filtered_inputs[i]->getType() == OpType::NOT) {
			auto *node = static_cast<NotNode *>(filtered_inputs[i]);
			bool found = false;
			for (unsigned j = 0; j < filtered_inputs.size(); j++) {
				if (i == j)
					continue;
				if (equals(filtered_inputs[j], node->getInput())) {
					found = true;
					replacement = new ConstantNode(false);
					break;
				}
			}
			if (found)
				break;
		}
	}

	if (replacement != nullptr) {
		for (auto *node : filtered_inputs) {
			delete node;
		}
		return replacement;
	}

	// If length is 1 there is no need for a gate
	if (filtered_inputs.size() == 1)
		return filtered_inputs[0];
	return new AndNode(filtered_inputs);
}

std::vector<OpNode *> AndNode::getInputs() const
{
	return m_inputs;
}

OpType AndNode::getType() const
{
	return OpType::AND;
}

bool AndNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<AndNode *>(other);

	// Only works if there are not duplicates
	// But it's called only on simplified and gates for which this is guaranteed
	if (m_inputs.size() != node->getInputs().size())
		return false;
	for (auto *input : m_inputs) {
		bool found = false;
		for (unsigned i = 0; i < node->getInputs().size(); i++) {
			if (equals(input, node->getInputs()[i])) {
				found = true;
				break;
			}
		}
		if (!found)
			return false;
	}
	return true;
}
