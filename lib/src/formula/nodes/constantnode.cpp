#include "constantnode.hpp"

ConstantNode::ConstantNode(bool input) : m_input(input)
{
}

ConstantNode::~ConstantNode() = default;

std::string ConstantNode::toString() const
{
	if (m_input)
		return "1";
	else
		return "0";
}

OpNode *ConstantNode::simplify()
{
	return new ConstantNode(m_input);
}

bool ConstantNode::getValue() const
{
	return m_input;
}

OpType ConstantNode::getType() const
{
	return OpType::CONSTANT;
}

bool ConstantNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<ConstantNode *>(other);
	return m_input == node->getValue();
}
