#ifndef OPNODE_H
#define OPNODE_H

#include <string>

enum class OpType { INPUT, CONSTANT, OUTPUT, AND, OR, XOR, NOT, BUFFER };

class OpNode
{
public:
	OpNode();
	virtual ~OpNode() = 0;

	static bool equals(OpNode *lhs, OpNode *rhs);
	static int getPriority(OpType type);

	virtual std::string toString() const = 0;
	virtual OpNode *simplify() = 0;

	virtual OpType getType() const = 0;

protected:
	virtual bool isEqual(OpNode *other) const = 0;
};

#endif // OPNODE_H
