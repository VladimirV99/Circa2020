#include "buffernode.hpp"

#include <sstream>

#include "util/stringutil.hpp"

BufferNode::BufferNode(OpNode *input) : m_input(input)
{
}

BufferNode::~BufferNode()
{
	delete m_input;
}

std::string BufferNode::toString() const
{
	std::ostringstream os;
	os << StringUtil::parenthesize_if(
		m_input->toString(), getPriority(m_input->getType()) > getPriority(getType()));
	return os.str();
}

OpNode *BufferNode::simplify()
{
	return m_input->simplify();
}

OpNode *BufferNode::getInput() const
{
	return m_input;
}

OpType BufferNode::getType() const
{
	return OpType::BUFFER;
}

bool BufferNode::isEqual(OpNode *other) const
{
	auto *node = static_cast<BufferNode *>(other);
	return equals(m_input, node->getInput());
}
