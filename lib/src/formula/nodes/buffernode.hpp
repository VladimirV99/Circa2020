#ifndef BUFFERNODE_H
#define BUFFERNODE_H

#include "opnode.hpp"

class BufferNode : public OpNode
{
public:
	BufferNode(OpNode *input);
	~BufferNode() override;

	std::string toString() const override;
	OpNode *simplify() override;

	OpNode *getInput() const;

	OpType getType() const override;

protected:
	bool isEqual(OpNode *other) const override;

private:
	OpNode *m_input;
};

#endif // BUFFERNODE_H
