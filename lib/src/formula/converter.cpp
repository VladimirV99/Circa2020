#include "converter.hpp"

#include "nodes/andnode.hpp"
#include "nodes/buffernode.hpp"
#include "nodes/constantnode.hpp"
#include "nodes/inputnode.hpp"
#include "nodes/notnode.hpp"
#include "nodes/ornode.hpp"
#include "nodes/outputnode.hpp"
#include "nodes/xornode.hpp"
#include "util/stringutil.hpp"

Converter::Converter() = default;

std::optional<std::vector<OpNode *>> Converter::toExpressionTrees(CircuitBoard &board)
{
	std::vector<OpNode *> result;
	for (auto pair : board.getAll()) {
		Component *c = pair.second;
		if (c->getType() == ComponentTypes::LIGHTBULB) {
			std::optional<OpNode *> output_node = inputDfs(c);
			if (!output_node) {
				std::cerr << "Error parsing formula" << std::endl;
				return std::nullopt;
			}
			result.push_back(output_node.value());
		}
	}
	return result;
}

std::vector<OpNode *> Converter::simplifyTrees(std::vector<OpNode *> trees)
{
	std::vector<OpNode *> result;
	for (auto *tree : trees) {
		result.push_back(tree->simplify());
	}
	return result;
}

std::vector<std::string> Converter::toFormulae(std::vector<OpNode *> trees)
{
	std::vector<std::string> result;
	for (auto *tree : trees) {
		result.push_back(tree->toString());
	}
	return result;
}

std::optional<OpNode *> Converter::inputDfs(Component *c)
{
	std::set<Component *> visited;
	return inputDfs(c, visited);
}

std::optional<OpNode *> Converter::inputDfs(Component *c, std::set<Component *> visited)
{
	if (visited.find(c) != visited.end()) {
		std::cerr << "Error: infinite loop" << std::endl;
		return std::nullopt;
	}

	visited.insert(c);

	std::optional<OpNode *> node = std::nullopt;

	switch (c->getType()) {
	case ComponentTypes::LIGHTBULB:
		node = makeSingleInputNode(OpType::OUTPUT, c, visited);
		break;
	case ComponentTypes::CONSTANT:
		node = new ConstantNode(true);
		break;
	case ComponentTypes::PIN:
		node = new InputNode(c->getId());
		break;
	case ComponentTypes::ANDGATE:
		node = makeMultiInputNode(OpType::AND, c, visited);
		break;
	case ComponentTypes::ORGATE:
		node = makeMultiInputNode(OpType::OR, c, visited);
		break;
	case ComponentTypes::XORGATE:
		node = makeMultiInputNode(OpType::XOR, c, visited);
		break;
	case ComponentTypes::NANDGATE: {
		std::optional<OpNode *> subnode = makeMultiInputNode(OpType::AND, c, visited);
		if (subnode.has_value())
			node = new NotNode(subnode.value());
		else
			node = std::nullopt;
		break;
	}
	case ComponentTypes::NORGATE: {
		std::optional<OpNode *> subnode = makeMultiInputNode(OpType::OR, c, visited);
		if (subnode.has_value())
			node = new NotNode(subnode.value());
		else
			node = std::nullopt;
		break;
	}
	case ComponentTypes::XNORGATE: {
		std::optional<OpNode *> subnode = makeMultiInputNode(OpType::XOR, c, visited);
		if (subnode.has_value())
			node = new NotNode(subnode.value());
		else
			node = std::nullopt;
		break;
	}
	case ComponentTypes::NOTGATE:
		node = makeSingleInputNode(OpType::NOT, c, visited);
		break;
	case ComponentTypes::BUFFER:
	case ComponentTypes::SPLITTER:
		node = makeSingleInputNode(OpType::BUFFER, c, visited);
		break;
	default:
		std::cerr << "Unsupported component" << std::endl;
		node = std::nullopt;
	}

	visited.erase(c);

	return node;
}

std::optional<OpNode *> Converter::makeSingleInputNode(OpType type, Component *c, std::set<Component *> visited)
{
	if (type != OpType::NOT && type != OpType::OUTPUT && type != OpType::BUFFER)
		return std::nullopt;

	std::vector<Component *> input_components;

	auto input_connection = c->getInputConnections()[0];
	if (input_connection) {
		getTransitiveInputs(static_cast<Wire *>(input_connection.to_component), input_components);
		if (input_components.size() > 1) {
			std::cerr << "Error: connected to too many components" << std::endl;
			return std::nullopt;
		} else if (input_components.empty()) {
			std::cerr << "Error: input is undefined";
			return std::nullopt;
		} else {
			std::optional<OpNode *> child = inputDfs(input_components[0], visited);
			if (child.has_value()) {
				OpNode *node;
				switch (type) {
				case OpType::NOT:
					node = new NotNode(child.value());
					break;
				case OpType::OUTPUT:
					node = new OutputNode(c->getId(), child.value());
					break;
				case OpType::BUFFER:
					node = new BufferNode(child.value());
					break;
				default:
					return std::nullopt;
				}
				return node;
			}
			return std::nullopt;
		}
	} else {
		std::cerr << "Error: input is undefined";
		return std::nullopt;
	}
}

std::optional<OpNode *> Converter::makeMultiInputNode(OpType type, Component *c, std::set<Component *> visited)
{
	if (type != OpType::AND && type != OpType::OR && type != OpType::XOR)
		return std::nullopt;

	std::vector<OpNode *> input_nodes;

	for (auto &input : c->getInputConnections()) {
		if (!input) {
			return std::nullopt;
		}
	}

	bool error = false;
	for (auto &input : c->getInputConnections()) {
		std::vector<Component *> input_components;
		getTransitiveInputs(static_cast<Wire *>(input.to_component), input_components);
		if (input_components.size() > 1) {
			std::cerr << "Error: connected to too many components" << std::endl;
			error = true;
			break;
		} else if (input_components.empty()) {
			std::cerr << "Error: input is undefined";
			error = true;
			break;
		} else {
			auto input_formula = inputDfs(input_components[0], visited);
			if (!input_formula.has_value()) {
				error = true;
				break;
			}
			input_nodes.push_back(input_formula.value());
		}
	}

	if (error) {
		for (auto *node : input_nodes) {
			delete node;
		}
	} else {
		OpNode *node;
		switch (type) {
		case OpType::AND:
			node = new AndNode(input_nodes);
			break;
		case OpType::OR:
			node = new OrNode(input_nodes);
			break;
		case OpType::XOR:
			node = new XorNode(input_nodes);
			break;
		default:
			node = nullptr;
		}
		if (node == nullptr)
			return std::nullopt;
		return node;
	}

	return std::nullopt;
}

void Converter::getTransitiveInputs(Wire *w, std::vector<Component *> &result)
{
	std::set<Wire *> visited;
	getTransitiveInputs(w, visited, result);
}

void Converter::getTransitiveInputs(Wire *w, std::set<Wire *> &visited, std::vector<Component *> &result)
{
	if (visited.find(w) != visited.end())
		return;

	visited.insert(w);

	for (auto input_connection : w->getInputConnections()) {
		if (input_connection) {
			auto *input_component = input_connection.to_component;
			if (input_component->getType() != ComponentTypes::WIRE) {
				result.push_back(input_component);
			} else {
				getTransitiveInputs(static_cast<Wire *>(input_component), visited, result);
			}
		}
	}
}
