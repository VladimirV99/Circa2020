#ifndef CONVERTER_H
#define CONVERTER_H

#include <optional>
#include <set>
#include <string>
#include <vector>

#include "circuitboard.hpp"
#include "nodes/opnode.hpp"
#include "wiring/wire.hpp"

class Converter
{
public:
	static std::optional<std::vector<OpNode *>> toExpressionTrees(CircuitBoard &board);
	static std::vector<OpNode *> simplifyTrees(std::vector<OpNode *> trees);
	static std::vector<std::string> toFormulae(std::vector<OpNode *> trees);

private:
	static std::optional<OpNode *> inputDfs(Component *c);
	static std::optional<OpNode *> inputDfs(Component *c, std::set<Component *> visited);

	static std::optional<OpNode *> makeSingleInputNode(OpType type, Component *c, std::set<Component *> visited);
	static std::optional<OpNode *> makeMultiInputNode(OpType type, Component *c, std::set<Component *> visited);

	static void getTransitiveInputs(Wire *w, std::vector<Component *> &result);
	static void getTransitiveInputs(Wire *w, std::set<Wire *> &visited, std::vector<Component *> &result);

	Converter();
};

#endif // CONVERTER_H
