#include "notgate.hpp"

NotGate::NotGate() : Gate(1)
{
}

NotGate::~NotGate() = default;

Signal NotGate::op()
{
	return !input_connections[0].getSignal();
}

void NotGate::resizeInputs(unsigned n)
{
	if (n != 1)
		throw "Not gate must have just one input!";
}

ComponentTypes NotGate::getType() const
{
	return ComponentTypes::NOTGATE;
}
