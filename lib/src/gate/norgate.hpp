#ifndef NORGATE_H
#define NORGATE_H

#include "gate.hpp"

class NorGate : public Gate
{
public:
	NorGate(unsigned nInputs = 2);
	~NorGate() override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // NORGATE_H
