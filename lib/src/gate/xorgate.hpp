#ifndef XORGATE_H
#define XORGATE_H
#include "gate.hpp"

class XorGate : public Gate
{
public:
	XorGate(unsigned nInputs = 2);
	~XorGate() override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // XORGATE_H
