#include "norgate.hpp"

#include "gate.hpp"

NorGate::NorGate(unsigned nInputs) : Gate(nInputs)
{
}

NorGate::~NorGate() = default;

Signal NorGate::op()
{
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::ERROR)
			return Signal(Signal::State::ERROR);
	}

	bool has_undefined = false;
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::TRUE)
			return Signal(Signal::State::FALSE);

		if (input.getSignal().getState() == Signal::State::UNDEFINED)
			has_undefined = true;
	}
	if (has_undefined)
		return Signal(Signal::State::UNDEFINED);

	return Signal(Signal::State::TRUE);
}

ComponentTypes NorGate::getType() const
{
	return ComponentTypes::NORGATE;
}
