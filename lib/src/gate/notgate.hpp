#ifndef NOTGATE_H
#define NOTGATE_H

#include "gate.hpp"

class NotGate : public Gate
{
public:
	NotGate();
	~NotGate() override;

	void resizeInputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // NOTGATE_H
