#include "andgate.hpp"

AndGate::AndGate(unsigned nInputs) : Gate(nInputs)
{
}

AndGate::~AndGate() = default;

Signal AndGate::op()
{
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::ERROR)
			return Signal(Signal::State::ERROR);
	}

	bool has_undefined = false;
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::FALSE)
			return Signal(Signal::State::FALSE);

		if (input.getSignal().getState() == Signal::State::UNDEFINED)
			has_undefined = true;
	}
	if (has_undefined)
		return Signal(Signal::State::UNDEFINED);

	return Signal(Signal::State::TRUE);
}

ComponentTypes AndGate::getType() const
{
	return ComponentTypes::ANDGATE;
}
