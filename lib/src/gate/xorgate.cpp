#include "xorgate.hpp"

#include <algorithm>

XorGate::XorGate(unsigned nInputs) : Gate(nInputs)
{
}

XorGate::~XorGate() = default;

Signal XorGate::op()
{

	if (std::any_of(std::begin(input_connections), std::end(input_connections), [](const Connection s) {
			return s.getSignal().getState() == Signal::State::ERROR;
		}))
		return Signal(Signal::State::ERROR);

	if (std::any_of(std::begin(input_connections), std::end(input_connections), [](const Connection &s) {
			return s.getSignal().getState() == Signal::State::UNDEFINED;
		}))
		return Signal(Signal::State::UNDEFINED);

	auto _true =
		std::count_if(std::begin(input_connections), std::end(input_connections), [](const Connection &s) {
			return s.getSignal().getState() == Signal::State::TRUE;
		});

	if (_true % 2 == 0)
		return Signal(Signal::State::FALSE);

	return Signal(Signal::State::TRUE);
}

ComponentTypes XorGate::getType() const
{
	return ComponentTypes::XORGATE;
}
