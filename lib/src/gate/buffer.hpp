#ifndef BUFFERGATE_H
#define BUFFERGATE_H

#include "gate.hpp"

class Buffer : public Gate
{
public:
	Buffer();
	~Buffer() override;

	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // BUFFERGATE_H
