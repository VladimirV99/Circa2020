#include "orgate.hpp"

OrGate::OrGate(unsigned nInputs) : Gate(nInputs)
{
}

OrGate::~OrGate() = default;
;

Signal OrGate::op()
{
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::ERROR)
			return Signal(Signal::State::ERROR);
	}

	bool has_undefined = false;
	for (const auto &input : this->input_connections) {
		if (input.getSignal().getState() == Signal::State::TRUE)
			return Signal(Signal::State::TRUE);

		if (input.getSignal().getState() == Signal::State::UNDEFINED)
			has_undefined = true;
	}
	if (has_undefined)
		return Signal(Signal::State::UNDEFINED);

	return Signal(Signal::State::FALSE);
}

ComponentTypes OrGate::getType() const
{
	return ComponentTypes::ORGATE;
}
