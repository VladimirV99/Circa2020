#ifndef NANDGATE_H
#define NANDGATE_H

#include "gate.hpp"

class NandGate : public Gate
{
public:
	NandGate(unsigned nInputs = 2);
	~NandGate() override;

	ComponentTypes getType() const override;

protected:
	Signal op() override;
};

#endif // NANDGATE_H
