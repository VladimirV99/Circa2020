#include "circuitboard.hpp"

#include "component/componentfactory.hpp"

CircuitBoard::CircuitBoard() = default;

CircuitBoard::~CircuitBoard()
{
	removeAll();
}

void CircuitBoard::add(Component *c, bool newId)
{
	if (newId) {
		int id = nextId();
		c->setId(id);
	}
	m_components.insert(std::make_pair(c->getId(), c));
}

Component *CircuitBoard::get(int id)
{
	auto it = m_components.find(id);
	if (it == m_components.cend())
		return nullptr;
	return it->second;
}

std::map<int, Component *> CircuitBoard::getAll()
{
	return m_components;
}

void CircuitBoard::remove(Component *c)
{
	const auto it = m_components.find(c->getId());
	if (it != m_components.cend()) {
		ConnectionManager::Instance().disconnectInputs(it->second);
		ConnectionManager::Instance().disconnectOutputs(it->second);
		delete it->second;
		m_components.erase(it);
	}
}

void CircuitBoard::removeAll()
{
	for (auto pair : m_components) {
		delete pair.second;
	}
	m_components.clear();
	m_currentId = 0;
}

void CircuitBoard::serializeBoard(std::ostream &os) const
{
	size_t n = m_components.size();
	os.write((char *)(&n), sizeof(n));

	for (auto pair : m_components) {
		Component *c = pair.second;
		ComponentType::serialize(os, c->getType());
		int id = c->getId();
		os.write((char *)(&id), sizeof(id));
	}

	for (auto pair : m_components) {
		Component *c = pair.second;
		c->serialize(os);
	}
}

void CircuitBoard::deserializeBoard(std::istream &is)
{
	removeAll();

	size_t nComponents;
	is.read((char *)(&nComponents), sizeof(nComponents));

	for (size_t i = 0; i < nComponents; i++) {
		ComponentTypes type = ComponentType::deserialize(is);
		int id;
		is.read((char *)(&id), sizeof(id));
		if (id >= m_currentId)
			m_currentId = id + 1;
		Component *c = ComponentFactory::instance().make(type);
		c->setId(id);
		add(c, false);
	}

	for (auto pair : m_components) {
		Component *c = pair.second;
		c->deserialize(is, m_components);
	}
}

int CircuitBoard::nextId()
{
	return m_currentId++;
}
