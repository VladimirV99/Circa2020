#include "constant.hpp"

Constant::Constant(const Signal::State &state) : Component(0, 1), m_state(state)
{
	this->output_connections[0].setSignal(m_state);
}

Constant::~Constant() = default;

void Constant::resizeInputs(unsigned int /*unused*/)
{
	throw "Constant doesn't have any inputs!";
}

void Constant::resizeOutputs(unsigned int /*unused*/)
{
	throw "Constant must have exactly one output!";
}

void Constant::update()
{
	this->output_connections[0].setSignal(m_state);
}

void Constant::serialize(std::ostream &os) const
{
	Component::serialize(os);
	int s = Signal::toInt(m_state);
	os.write((char *)(&s), sizeof(s));
}

void Constant::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	Component::deserialize(is, lookup);
	int s;
	is.read((char *)(&s), sizeof(s));
	m_state = Signal::fromInt(s);
}

ComponentTypes Constant::getType() const
{
	return ComponentTypes::CONSTANT;
}
