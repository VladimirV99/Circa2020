#ifndef CONSTANT_H
#define CONSTANT_H

#include "gate/gate.hpp"

class Constant : public Component
{
public:
	Constant(const Signal::State &);
	~Constant() override;

	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;
	void update() override;

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is, std::map<int, Component *> lookup) override;

	ComponentTypes getType() const override;

private:
	Signal::State m_state;
};

#endif // CONSTANT_H
