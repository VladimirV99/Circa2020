#ifndef PIN_H
#define PIN_H

#include "component/component.hpp"

class Pin : public Component
{
public:
	Pin(const Signal::State &);
	Pin();
	~Pin() override;

	void flip();
	void setState(Signal::State);
	Signal::State getState();
	void resizeOutputs(unsigned n) override;
	void resizeInputs(unsigned n) override;
	void update() override;

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is, std::map<int, Component *> lookup) override;

	ComponentTypes getType() const override;

private:
	Signal::State m_state;
};

#endif // PIN_H
