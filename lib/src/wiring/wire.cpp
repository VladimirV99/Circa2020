#include "wire.hpp"

Wire::Wire() : Component(6, 6), m_state(Signal::State::UNDEFINED)
{
}

Wire::~Wire() = default;

void Wire::resizeInputs(unsigned n)
{
	Component::resizeInputs(n);
	if (getOutputSize() != n)
		resizeOutputs(n);
}

void Wire::resizeOutputs(unsigned n)
{
	Component::resizeOutputs(n);
	if (getInputSize() != n)
		resizeInputs(n);
}

void Wire::update()
{
	for (unsigned i = 0; i < getInputSize(); i++) {
		if (i < getOutputSize() && getInputConnections()[i] && getOutputConnections()[i]) {
			output_connections[i].setSignal(getInput(i));
			output_connections[i].flush();
		}
	}

	Signal::State newState = Signal::State::UNDEFINED;
	for (unsigned i = 0; i < getInputSize(); i++) {
		if (!input_connections[i])
			continue;
		bool shouldBreak = false;
		bool isFromWire = output_connections[i];
		if (input_connections[i].hasChanged() || (!input_connections[i].hasChanged() && !isFromWire)) {
			switch (input_connections[i].getSignal().getState()) {
			case Signal::State::ERROR:
				newState = Signal::State::ERROR;
				shouldBreak = true;
				break;
			case Signal::State::UNDEFINED:
				// dont change?
				break;
			default:
				if (newState != Signal::State::UNDEFINED &&
					newState != input_connections[i].getSignal().getState()) {
					newState = Signal::State::ERROR;
					shouldBreak = true;
				} else {
					newState = input_connections[i].getSignal().getState();
				}
				break;
			}
		}
		if (shouldBreak)
			break;
	}
	m_state = newState;
	for (auto &connection : output_connections)
		connection.setSignal(Signal(m_state));

	for (unsigned i = 0; i < getInputSize(); i++) {
		if (i < getOutputSize() && getInputConnections()[i] && getOutputConnections()[i]) {
			input_connections[i].setSignal(m_state);
			input_connections[i].flush();
		}
	}
}

void Wire::serialize(std::ostream &os) const
{
	Component::serialize(os);
	int s = Signal::toInt(m_state);
	os.write((char *)(&s), sizeof(s));
}

void Wire::deserialize(std::istream &is, std::map<int, Component *> lookup)
{
	Component::deserialize(is, lookup);
	int s;
	is.read((char *)(&s), sizeof(s));
	m_state = Signal::fromInt(s);
}

ComponentTypes Wire::getType() const
{
	return ComponentTypes::WIRE;
}

Signal::State Wire::getState() const
{
	return m_state;
}
