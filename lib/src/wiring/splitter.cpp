#include "splitter.hpp"

#include "connection/connectionmanager.hpp"

Splitter::Splitter() : Component(1, 4)
{
}

Splitter::~Splitter() = default;

void Splitter::resizeInputs(unsigned n)
{
	if (n != 1)
		throw "Splitter must have just one input!";
}

void Splitter::resizeOutputs(unsigned n)
{
	if (n == getOutputSize())
		return;

	if (n < getOutputSize())
		ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

	output_connections.resize(n, Connection());
	notifyStateChanged();
}

void Splitter::update()
{

	Signal sig = this->input_connections[0].getSignal();

	for (auto &conn : this->output_connections) {
		conn.setSignal(sig);
	}
}

ComponentTypes Splitter::getType() const
{
	return ComponentTypes::SPLITTER;
}
