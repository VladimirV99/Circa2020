#ifndef NEGATOR_H
#define NEGATOR_H

#include "component/component.hpp"

class Negator : public Component
{
public:
	Negator();
	~Negator() override = default;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // NEGATOR_H
