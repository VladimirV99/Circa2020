#ifndef SUBTRACTOR_H
#define SUBTRACTOR_H

#include "component/component.hpp"

class Subtractor : public Component
{
public:
	Subtractor();
	~Subtractor() override = default;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // SUBTRACTOR_H
