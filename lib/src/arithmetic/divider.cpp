#include "divider.hpp"

#include <cmath>

#include "connection/connectionmanager.hpp"

Divider::Divider() : Component(6, 3)
{
}

void Divider::resizeInputs(unsigned n)
{
	if (n == getInputSize())
		return;

	if (n % 2 != 0)
		throw "Divider must have even number of inputs!";

	if (n < getInputSize())
		ConnectionManager::Instance().disconnectInputs(this, n, getInputSize());

	input_connections.resize(n, Connection());

	if (n / 2 == getOutputSize())
		return;

	if (n / 2 < getOutputSize())
		ConnectionManager::Instance().disconnectOutputs(this, n, getOutputSize());

	output_connections.resize(n / 2, Connection());

	notifyStateChanged();
}

void Divider::resizeOutputs(unsigned /*unused*/)
{
	throw "Cannot resize outputs in divider!";
}

void Divider::update()
{

	size_t inputs_size = this->input_connections.size();

	if (inputs_size % 2 != 0) {
		for (auto &conn : this->output_connections)
			conn.setSignal(Signal(Signal::State::ERROR));
		return;
	}

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			for (auto &conn : this->output_connections)
				conn.setSignal(Signal(Signal::State::ERROR));
			return;
		}
	}

	for (auto &conn : this->input_connections) {
		if (conn.getSignal().getState() == Signal::State::UNDEFINED) {
			for (auto &conn : this->output_connections)
				conn.setSignal(Signal(Signal::State::UNDEFINED));
			return;
		}
	}

	int first_operand = 0;
	int second_operand = 0;

	int exponent = 0;
	for (size_t i = inputs_size / 2 - 1; i > 0; i--) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
			first_operand += std::pow(2, exponent);
		exponent += 1;
	}
	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE)
		first_operand = -first_operand;

	exponent = 0;
	for (size_t i = inputs_size - 1; i > inputs_size / 2; i--) {
		if (this->input_connections[i].getSignal().getState() == Signal::State::TRUE)
			second_operand += std::pow(2, exponent);
		exponent += 1;
	}
	if (this->input_connections[inputs_size / 2].getSignal().getState() == Signal::State::TRUE)
		second_operand = -second_operand;

	if (second_operand == 0)
		second_operand = 1;

	int result = first_operand / second_operand;

	std::vector<bool> bits = std::vector<bool>(this->output_connections.size());
	if (result >= 0) {
		bits[0] = false;
	} else {
		bits[0] = true;
	}

	for (size_t k = 0; k < this->output_connections.size() - 1; k++) {
		bits[this->output_connections.size() - k - 1] = ((result % 2) != 0);
		result /= 2;
	}

	for (size_t j = 0; j < this->output_connections.size(); j++) {
		this->output_connections[j].setSignal(bits[j] ? Signal::State::TRUE : Signal::State::FALSE);
	}
}

ComponentTypes Divider::getType() const
{
	return ComponentTypes::DIVIDER;
}
