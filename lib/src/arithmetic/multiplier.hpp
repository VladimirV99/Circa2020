#ifndef MULTIPLIER_H
#define MULTIPLIER_H

#include "component/component.hpp"

class Multiplier : public Component
{
public:
	Multiplier();
	~Multiplier() override = default;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // MULTIPLIER_H
