#ifndef CIRCUITBOARD_H
#define CIRCUITBOARD_H

#include <map>

#include "component/component.hpp"
#include "connection/connectionmanager.hpp"

class CircuitBoard
{
public:
	CircuitBoard();
	virtual ~CircuitBoard();

	// TODO Which methods should be virtual?
	virtual void add(Component *c, bool newId = true);
	Component *get(int id);
	std::map<int, Component *> getAll();
	virtual void remove(Component *c);
	virtual void removeAll();

	virtual void serializeBoard(std::ostream &os) const;
	virtual void deserializeBoard(std::istream &is);

protected:
	std::map<int, Component *> m_components;

private:
	int nextId();
	int m_currentId = 0;
};

#endif // CIRCUITBOARD_H
