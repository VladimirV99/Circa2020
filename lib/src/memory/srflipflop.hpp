#ifndef SRFLIPFLOP_H
#define SRFLIPFLOP_H

#include "component/component.hpp"

class SRFlipFlop : public Component
{
public:
	SRFlipFlop();
	~SRFlipFlop() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // SRFLIPFLOP_H
