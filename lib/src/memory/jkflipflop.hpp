#ifndef JKFLIPFLOP_H
#define JKFLIPFLOP_H

#include "component/component.hpp"

class JKFlipFlop : public Component
{
public:
	JKFlipFlop();
	~JKFlipFlop() override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n) override;

	ComponentTypes getType() const override;

protected:
	void update() override;
};

#endif // JKFLIPFLOP_H
