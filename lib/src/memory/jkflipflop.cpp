#include "jkflipflop.hpp"

JKFlipFlop::JKFlipFlop() : Component(2, 2)
{
}

JKFlipFlop::~JKFlipFlop() = default;

void JKFlipFlop::resizeInputs(unsigned n)
{
	if (n != 2)
		throw "Resize inputs for SR flip-flop is not allowed!";
}

void JKFlipFlop::resizeOutputs(unsigned n)
{
	if (n != 2)
		throw "Resize outputs for SR flip-flop is not allowed!";
}

void JKFlipFlop::update()
{

	for (auto &conn : input_connections) {
		if (conn.getSignal().getState() == Signal::State::ERROR) {
			output_connections[0].setSignal(Signal::State::ERROR);
			output_connections[1].setSignal(Signal::State::ERROR);
			return;
		}
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::FALSE &&
		this->input_connections[1].getSignal().getState() == Signal::State::FALSE)
		return;

	if (this->input_connections[0].getSignal().getState() == Signal::State::FALSE &&
		this->input_connections[1].getSignal().getState() == Signal::State::TRUE) {
		this->output_connections[0].setSignal(Signal::State::FALSE);
		this->output_connections[1].setSignal(Signal::State::TRUE);
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE &&
		this->input_connections[1].getSignal().getState() == Signal::State::FALSE) {
		this->output_connections[0].setSignal(Signal::State::TRUE);
		this->output_connections[1].setSignal(Signal::State::FALSE);
	}

	if (this->input_connections[0].getSignal().getState() == Signal::State::TRUE &&
		this->input_connections[1].getSignal().getState() == Signal::State::TRUE) {
		this->output_connections[0].setSignal(!output_connections[0].getSignal());
		this->output_connections[1].setSignal(!output_connections[1].getSignal());
	}
}

ComponentTypes JKFlipFlop::getType() const
{
	return ComponentTypes::JKFLIPFLOP;
}
