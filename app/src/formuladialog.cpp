#include "formuladialog.hpp"

#include <iostream>

#include <QClipboard>
#include <QFile>
#include <QFileDialog>

#include "ui_formuladialog.h"

FormulaDialog::FormulaDialog(std::string formulae, QWidget *parent) : QDialog(parent), ui(new Ui::FormulaDialog)
{
	setWindowTitle("Formula");

	ui->setupUi(this);

	ui->lblFormulae->setText(QString::fromStdString(formulae));

	connect(ui->bCopy, SIGNAL(clicked()), this, SLOT(onCopyTriggered()));
	connect(ui->bSave, SIGNAL(clicked()), this, SLOT(onSaveTriggered()));
	connect(ui->bClose, SIGNAL(clicked()), this, SLOT(onCloseTriggered()));
}

FormulaDialog::~FormulaDialog()
{
	delete ui;
}

void FormulaDialog::onCopyTriggered()
{
	QClipboard *clipboard = QApplication::clipboard();

	clipboard->setText(ui->lblFormulae->text(), QClipboard::Clipboard);

	if (clipboard->supportsSelection()) {
		clipboard->setText(ui->lblFormulae->text(), QClipboard::Selection);
	}
}

void FormulaDialog::onSaveTriggered()
{
	QString defaultPath = QDir::currentPath() + "/untitled.txt";

	QString fileName =
		QFileDialog::getSaveFileName(this, tr("Save formulae"), defaultPath, tr("Text file (*.txt)"));
	if (fileName.isEmpty())
		return;
	else {
		QFile file(fileName);
		if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
			file.write(ui->lblFormulae->text().toUtf8());
			file.close();
		}
	}
}

void FormulaDialog::onCloseTriggered()
{
	close();
}
