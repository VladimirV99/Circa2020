#ifndef SCENE_H
#define SCENE_H

#include <cmath>
#include <iostream>
#include <limits>
#include <unordered_set>
#include <utility>

#include <QApplication>
#include <QDebug>
#include <QDrag>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QMimeData>
#include <QPainter>
#include <QTimer>

#include "circuitboard.hpp"
#include "component/component.hpp"
#include "util/clipboard.hpp"
#include "util/controlmode.hpp"
#include "util/direction.hpp"
#include "util/selectinfo.hpp"
#include "util/wiredrawinfo.hpp"
#include "util/wiredrawutils.hpp"
#include "wiring/wire.hpp"

class MainWindow;

class ComponentGraphicsItem;

class Scene : public QGraphicsScene
{
	Q_OBJECT
public:
	WireDrawInfo m_drawing_wire_info;
	SelectInfo m_select_info;

	static const int UNIT = 10;
	static const int POINT_SIZE = 8;

	explicit Scene(QObject *parent = nullptr);
	~Scene();

	QPointF m_startPoint;
	QPointF m_endPoint;

	static const std::pair<unsigned, unsigned> directionLookup[4][4];
	static QPointF mapToGrid(const QPointF &);
	static float mapToGrid(float);
	CircuitBoard m_cb;

	void resizeItemInputs(unsigned n);
	void changeOrientation(Direction orient);

	void serialize(std::ostream &os) const;
	void deserialize(std::istream &is);

	void reset();

	ControlMode controlMode();
	void setControlMode(const ControlMode &);

	QRectF getManualSelectionRect() const;
	void clearSelection();

	std::vector<ComponentGraphicsItem *> getItems();

	void deleteComponent(ComponentGraphicsItem *item);
	void addComponent(ComponentGraphicsItem *item);

	void setChanged(bool flag);
	bool isChanged() const;
	MainWindow *getMainWindow();

protected:
	void drawBackground(QPainter *painter, const QRectF &rect) override;
	void drawForeground(QPainter *painter, const QRectF &rect) override;

	void dragEnterEvent(QGraphicsSceneDragDropEvent *event) override;
	void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
	void dropEvent(QGraphicsSceneDragDropEvent *event) override;

	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
	void designModeMousePressEvent(QGraphicsSceneMouseEvent *event);
	void selectionModeMousePressEvent(QGraphicsSceneMouseEvent *event);

	void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
	void designModeMouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void selectionModeMouseMoveEvent(QGraphicsSceneMouseEvent *event);

	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
	void selectionModeMouseReleaseEvent(QGraphicsSceneMouseEvent *event);

	void keyPressEvent(QKeyEvent *event) override;
	void keyReleaseEvent(QKeyEvent *event) override;

	friend class MainWindow;

private:
	MainWindow *mw;
	ClipBoard m_clipBoard;

	std::vector<IndirectClickReport> clickedChildren(const QPointF &clickPoint);
	void processReport(
		const std::vector<IndirectClickReport> &report, QGraphicsSceneMouseEvent *event,
		const QPointF &clickPoint);

	void resetDrawInfo();
	void resetSelectInfo();
	void deleteSelection();

	QPointF modifiedScenePos(const QPointF &event) const;

	ControlMode m_controlMode;

	QTimer *m_clockTimer;

	mutable bool m_changed;

signals:
	void toggleBox(bool isScenePressed, const QString &boxName, unsigned input_size, int direction);

public slots:
	void tick();
};

#endif // SCENE_H
