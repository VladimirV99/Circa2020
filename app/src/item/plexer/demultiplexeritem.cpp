#include "demultiplexeritem.hpp"

DemultiplexerItem::DemultiplexerItem(int posX, int posY, QGraphicsItem *parent)
	: DemultiplexerItem(ComponentFactory::instance().makeDemultiplexer(), posX, posY, parent)
{
}

DemultiplexerItem::DemultiplexerItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::DEMULTIPLEXER)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getInputSize() - 2);
}

int DemultiplexerItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT) {
		return m_component->getOutputSize() * 2;
	} else {
		int width = (m_component->getInputSize() - 2) * 2;
		if (width < 7)
			return 7;
		return (m_component->getInputSize() - 2) * 2;
	}
}

int DemultiplexerItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT) {
		int width = (m_component->getInputSize() - 2) * 2;
		if (width < 7)
			return 7;
		return width;
	} else {
		int width = m_component->getOutputSize() * 2;
		if (width < 7)
			return 7;
		return width;
	}
}

void DemultiplexerItem::drawOrientation()
{
	prepareGeometryChange();

	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();
	m_inputPoints = {};
	m_outputPoints = {};

	if (m_orientation == Direction::RIGHT) {
		for (unsigned i = 0; i < m_component->getInputSize() - 2; i++)
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

		m_inputPoints.append(ConnectionPoint(0, m_height / 3, Direction::LEFT));
		m_inputPoints.append(ConnectionPoint(0, 2 * m_height / 3, Direction::LEFT));

		m_outputPoints = {};

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));

	} else if (m_orientation == Direction::DOWN) {
		for (unsigned i = 0; i < m_component->getInputSize() - 2; i++)
			m_inputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::DOWN));

		m_inputPoints.append(ConnectionPoint(m_width / 3, 0, Direction::UP));
		m_inputPoints.append(ConnectionPoint(2 * m_width / 3, 0, Direction::UP));

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

	} else if (m_orientation == Direction::LEFT) {
		for (int i = m_component->getInputSize() - 3; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));

		m_inputPoints.append(ConnectionPoint(m_width, m_height / 3, Direction::RIGHT));
		m_inputPoints.append(ConnectionPoint(m_width, 2 * m_height / 3, Direction::RIGHT));

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));

	} else {
		for (int i = m_component->getInputSize() - 3; i >= 0; i--)
			m_inputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));

		m_inputPoints.append(ConnectionPoint(0, m_height / 3, Direction::LEFT));
		m_inputPoints.append(ConnectionPoint(0, 2 * m_height / 3, Direction::LEFT));

		for (unsigned i = 0; i < m_component->getOutputSize(); i++)
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));
	}
}

void DemultiplexerItem::resizeInputs(unsigned n)
{
	this->m_component->resizeInputs(n);
	drawOrientation();
}

void DemultiplexerItem::calculatePath()
{
	m_path = QPainterPath(QPoint(0, 0));
	m_path.lineTo(0, m_height);
	m_path.lineTo(m_width, m_height);
	m_path.lineTo(m_width, 0);
	m_path.lineTo(0, 0);
	m_path.addText(QPoint(Scene::UNIT, Scene::UNIT * 2), QFont("Times", 10), QString("DEMUX"));
}

void DemultiplexerItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	painter->setBrush(b);

	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
