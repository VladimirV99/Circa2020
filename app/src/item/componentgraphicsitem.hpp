#ifndef COMPONENTGRAPHICSITEM_H
#define COMPONENTGRAPHICSITEM_H

#include <string>

#include <QBrush>
#include <QCursor>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QMap>
#include <QMouseEvent>
#include <QPaintEngine>

#include "component/component.hpp"
#include "component/componentfactory.hpp"
#include "scene.hpp"
#include "util/connectionpoint.hpp"
#include "util/misc.hpp"
#include "util/wiredrawutils.hpp"

class ComponentGraphicsItem : public QGraphicsItem
{
public:
	ComponentGraphicsItem(
		int posX, int posY, int width, int height, Component *component, QGraphicsItem *parent = nullptr,
		Direction orient = Direction::RIGHT);
	~ComponentGraphicsItem() override;
	QRectF boundingRect() const override;
	virtual QRectF overlapBoundingRect() const;
	virtual QRectF overlapBoundingRectSc() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

	Component *m_component;

	QVector<ConnectionPoint> getInputPoints() const;
	QVector<ConnectionPoint> getOutputPoints() const;

	bool getIsResizable() const;
	Direction getOrientation();

	void processMousePressed(QGraphicsSceneMouseEvent *event);
	virtual void resizeInputs(unsigned n);
	void safeResize(unsigned n);

	void safeRotate(const Direction &orient);
	virtual void changeOrientation(Direction orient);

	virtual void drawOrientation() {};

	virtual void serialize(std::ostream &os) const;
	virtual void deserialize(std::istream &is);
	bool inCache(ComponentGraphicsItem *item);

protected:
	void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
	void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
	void hoverMoveEvent(QGraphicsSceneHoverEvent *event) override;

	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
	void designModeMousePressEvent(QGraphicsSceneMouseEvent *event);
	void selectionModeMousePressEvent(QGraphicsSceneMouseEvent *event);

	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
	void designModeMouseReleaseEvent(QGraphicsSceneMouseEvent *event);
	void selectionModeMouseReleaseEvent(QGraphicsSceneMouseEvent *event);

	void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
	void designModeMouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void selectionModeMouseMoveEvent(QGraphicsSceneMouseEvent *event);

	Scene *getScene();

	QPen outlinePen;
	QBrush fillBrush;

	int m_width;
	int m_height;

	QPointF m_previousClickPoint;

	bool m_isHolding;
	bool m_isHovering;
	bool m_isResizable;
	Direction m_orientation;

	bool m_isInSelection;

	QVector<ConnectionPoint> m_inputPoints;
	QVector<ConnectionPoint> m_outputPoints;

	void fillWireItemCache();
	struct WireItemCacheEntry {
		unsigned m_idx;
		bool m_is_output;
		WireItem *m_to_wireitem;
		WireItemCacheEntry(unsigned idx, bool is_output, WireItem *to_wireitem)
			: m_idx(idx), m_is_output(is_output), m_to_wireitem(to_wireitem)
		{
		}
	};

	struct WireItemCacheEntryDelayedAction {
		WireItem *m_wireitem;
		int m_new_x;
		int m_new_y;
		int m_new_width;
		int m_new_height;
		WireItemCacheEntryDelayedAction(WireItem *wireitem, int new_x, int new_y, int new_width, int new_height)
			: m_wireitem(wireitem), m_new_x(new_x), m_new_y(new_y), m_new_width(new_width),
			  m_new_height(new_height)
		{
		}
	};

	bool m_wireItemCacheValid;
	std::vector<WireItemCacheEntry> m_wireItemCache;

	friend class ItemFactory;
	friend class Scene;
};

#endif // COMPONENTGRAPHICSITEM_H
