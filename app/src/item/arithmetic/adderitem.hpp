#ifndef ADDERITEM_H
#define ADDERITEM_H

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"
#include "util/orientation.hpp"

class AdderItem : public ComponentGraphicsItem
{
public:
	AdderItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	AdderItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

	void resizeInputs(unsigned n) override;
	void resizeOutputs(unsigned n);
	void drawOrientation() override;

protected:
	int calculateWidth() const;
	int calculateHeight() const;
	void calculatePath();

	QPainterPath m_path;
};

#endif // ADDERITEM_H
