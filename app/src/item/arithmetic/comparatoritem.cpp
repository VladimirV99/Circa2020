#include "comparatoritem.hpp"

#include <QMessageBox>

ComparatorItem::ComparatorItem(int posX, int posY, QGraphicsItem *parent)
	: ComparatorItem(ComponentFactory::instance().makeComparator(), posX, posY, parent)
{
}

ComparatorItem::ComparatorItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, 0, 0, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::COMPARATOR)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getInputSize());
}

int ComparatorItem::calculateHeight() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT) {
		if (m_component->getInputSize() == 2)
			return m_component->getOutputSize() * 2;

		return m_component->getInputSize() * 2;
	} else {
		return 5;
	}
}

int ComparatorItem::calculateWidth() const
{
	if (m_orientation == Direction::LEFT || m_orientation == Direction::RIGHT) {
		return 5;
	} else {
		if (m_component->getInputSize() == 2)
			return m_component->getOutputSize() * 2;

		return m_component->getInputSize() * 2;
	}
}

void ComparatorItem::resizeInputs(unsigned n)
{
	if (n % 2 == 1) {
		QMessageBox::warning(nullptr, QString("Error"), QString("Comparator must have even number of inputs!"));
		return;
	}
	this->m_component->resizeInputs(n);
	drawOrientation();
}

void ComparatorItem::drawOrientation()
{
	prepareGeometryChange();

	m_height = calculateHeight() * Scene::UNIT;
	m_width = calculateWidth() * Scene::UNIT;

	calculatePath();
	m_inputPoints = {};
	m_outputPoints = {};

	if (m_orientation == Direction::RIGHT) {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));
		}
		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));
		}
	} else if (m_orientation == Direction::DOWN) {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));
		}
		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));
		}
	} else if (m_orientation == Direction::LEFT) {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint(m_width, (2 * i + 1) * Scene::UNIT, Direction::RIGHT));
		}
		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint(0, (2 * i + 1) * Scene::UNIT, Direction::LEFT));
		}
	} else {
		for (unsigned i = 0; i < m_component->getInputSize(); i++) {
			m_inputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, m_height, Direction::DOWN));
		}
		for (unsigned i = 0; i < m_component->getOutputSize(); i++) {
			m_outputPoints.append(ConnectionPoint((2 * i + 1) * Scene::UNIT, 0, Direction::UP));
		}
	}
}

void ComparatorItem::calculatePath()
{
	m_path = QPainterPath(QPoint(0, 0));
	m_path.lineTo(0, m_height);
	m_path.lineTo(m_width, m_height);
	m_path.lineTo(m_width, 0);
	m_path.lineTo(0, 0);
	m_path.addText(QPoint(Scene::UNIT * 3, Scene::UNIT * 2), QFont("Times", 20, QFont::Bold), QString(">"));
	m_path.addText(QPoint(Scene::UNIT * 3, Scene::UNIT * 3.75), QFont("Times", 20, QFont::Bold), QString("="));
	m_path.addText(QPoint(Scene::UNIT * 3, Scene::UNIT * 5.75), QFont("Times", 20, QFont::Bold), QString("<"));
	if (m_orientation == Direction::RIGHT) {
		m_path.addText(QPoint(Scene::UNIT / 2, Scene::UNIT / 2), QFont("Times", 20, QFont::Bold), QString("."));
	} else if (m_orientation == Direction::LEFT) {
		m_path.addText(
			QPoint(m_width - Scene::UNIT, Scene::UNIT / 2), QFont("Times", 20, QFont::Bold), QString("."));
	} else if (m_orientation == Direction::DOWN) {
		m_path.addText(QPoint(Scene::UNIT / 2, Scene::UNIT), QFont("Times", 20, QFont::Bold), QString("."));
	} else {
		m_path.addText(
			QPoint(Scene::UNIT / 2, m_height - Scene::UNIT), QFont("Times", 20, QFont::Bold), QString("."));
	}
}

void ComparatorItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	// Bounds
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	// Shape
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	painter->setBrush(b);

	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
