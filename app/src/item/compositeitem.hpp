#ifndef COMPOSITEITEM_H
#define COMPOSITEITEM_H

#include "componentgraphicsitem.hpp"

class CompositeItem : public ComponentGraphicsItem
{
public:
	CompositeItem(
		int posX, int posY, std::vector<Pin *> pins, std::vector<LightBulb *> lbs, std::string name,
		QGraphicsItem *parent = nullptr);

	CompositeItem(
		Component *component, int posX = 0, int posY = 0, std::string name = "Unnamed",
		QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

	void setName(const std::string &name);

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is) override;

protected:
	void resizeInputs(unsigned n) override;
	int calculateWidth() const;
	int calculateHeight() const;
	void calculatePath();

	QPainterPath m_path;
	std::string m_name;
};

#endif // COMPOSITEITEM_H
