#include "item/wiring/constantitem.hpp"

ConstantItem::ConstantItem(int posX, int posY, int width, QGraphicsItem *parent)
	: ConstantItem(posX, posY, width, ComponentFactory::instance().makeConstant(Signal::State::TRUE), parent)
{
}

ConstantItem::ConstantItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, width, width, component, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::CONSTANT)
		throw "Incompatible component for this item";
	m_isResizable = false;
	drawOrientation();
}

void ConstantItem::drawOrientation()
{
	if (m_orientation == Direction::RIGHT) {
		m_outputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};
	} else if (m_orientation == Direction::DOWN) {
		m_outputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};
	} else if (m_orientation == Direction::LEFT) {
		m_outputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};
	} else {
		m_outputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};
	}
}

void ConstantItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	//   painter->setPen(Qt::yellow);
	//   painter->drawRect(boundingRect());

	QPen p = QPen(Qt::green);
	p.setWidth(3);
	painter->setPen(p);
	painter->drawLine(m_width / 8, 3 * m_height / 8, m_width / 2, 0);
	painter->drawLine(m_width / 2, 0, m_width / 2, m_height);

	ComponentGraphicsItem::paint(painter, option, widget);
}
