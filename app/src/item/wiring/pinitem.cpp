#include "item/wiring/pinitem.hpp"

#include "util/colorutil.hpp"
#include "wiring/clock.hpp"

PinItem::PinItem(int posX, int posY, int width, QGraphicsItem *parent)
	: PinItem(posX, posY, width, ComponentFactory::instance().makePin(Signal::State::FALSE), parent)
{
}

PinItem::PinItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent)
	: ComponentGraphicsItem(posX, posY, width, width, component, parent)
{
	if (component->getType() != ComponentTypes::PIN)
		throw "Incompatible component for this item";
	m_isResizable = false;
	drawOrientation();
}

void PinItem::drawOrientation()
{
	if (m_orientation == Direction::RIGHT) {
		m_outputPoints = {ConnectionPoint(m_width, m_height / 2, Direction::RIGHT)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	} else if (m_orientation == Direction::LEFT) {
		m_outputPoints = {ConnectionPoint(0, m_height / 2, Direction::LEFT)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	} else if (m_orientation == Direction::DOWN) {
		m_outputPoints = {ConnectionPoint(m_width / 2, m_height, Direction::DOWN)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	} else {
		m_outputPoints = {ConnectionPoint(m_width / 2, 0, Direction::UP)};
		flipSwitch = QRectF(m_width / 4, m_height / 4, m_width / 2, m_height / 2);
	}
}

void PinItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	//    painter->setPen(Qt::yellow);
	//    painter->drawRect(boundingRect());

	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	painter->drawRect(0, 0, m_width, m_height);
	QBrush b = QBrush(ColorUtil::forState(m_component->getOutput(0).getState()));
	painter->setBrush(b);

	painter->drawEllipse(m_width / 4, m_height / 4, m_width / 2, m_height / 2);

	ComponentGraphicsItem::paint(painter, option, widget);
}

void PinItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	if (getScene()->controlMode() == ControlMode::PINS && flipSwitch.contains(event->pos())) {
		static_cast<Clock *>(m_component)->tick();
		update(boundingRect());
	} else {
		ComponentGraphicsItem::mousePressEvent(event);
	}
}
