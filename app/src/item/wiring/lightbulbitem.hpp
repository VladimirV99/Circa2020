#ifndef LIGHTBULBITEM_H
#define LIGHTBULBITEM_H

#include "item/componentgraphicsitem.hpp"

class LightBulbItem : public ComponentGraphicsItem
{
public:
	LightBulbItem(int posX = 0, int posY = 0, int width = 2, QGraphicsItem *parent = nullptr);
	LightBulbItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

	void resizeInputs(unsigned n) override;

	void drawOrientation() override;
};

#endif // LIGHTBULBITEM_H
