#ifndef WIREITEM_H
#define WIREITEM_H

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"

class WireItem : public ComponentGraphicsItem
{
public:
	WireItem(QGraphicsItem *parent = nullptr);
	WireItem(
		int posX, int posY, int width, int height, Wire *component, const Direction &direction,
		QGraphicsItem *parent = nullptr);
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

	void serialize(std::ostream &os) const override;
	void deserialize(std::istream &is) override;

	std::tuple<WireItem *, Direction, Direction> splitMe(const QPointF &break_point);
	Direction getDirection();
	void refreshConnectionPoints();

private:
	Direction m_direction;
	Wire *m_wire;

protected:
	QRectF boundingRect() const override;
	QRectF overlapBoundingRect() const override;
	QRectF overlapBoundingRectSc() const override;

	friend class ItemFactory;
	friend class WireDrawUtils;
};

#endif // WIREITEM_H
