#ifndef CLOCKITEM_H
#define CLOCKITEM_H

#include "item/componentgraphicsitem.hpp"

class ClockItem : public ComponentGraphicsItem
{
public:
	ClockItem(int posX = 0, int posY = 0, int width = 2, QGraphicsItem *parent = nullptr);
	ClockItem(int posX, int posY, int width, Component *component, QGraphicsItem *parent = nullptr);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) override;

	void drawOrientation() override;

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event) override;

private:
	QRectF flipSwitch;

	QPainterPath pathOff;
	QPainterPath pathOn;
};

#endif // CLOCKITEM_H
