#ifndef NANDGATEITEM_H
#define NANDGATEITEM_H

#include "gateitem.hpp"

class NandGateItem : public GateItem
{
public:
	NandGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	NandGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
};

#endif // NANDGATEITEM_H
