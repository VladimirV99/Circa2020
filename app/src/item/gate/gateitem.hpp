#ifndef GATEITEM_H
#define GATEITEM_H

#include "item/componentgraphicsitem.hpp"
#include "util/direction.hpp"

class GateItem : public ComponentGraphicsItem
{
public:
	GateItem(
		Component *component, int posX, int posY, int width, int height, QGraphicsItem *parent = nullptr,
		Direction orient = Direction::RIGHT);

	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;

	void resizeInputs(unsigned n) override;
	void drawOrientation() override;

protected:
	// returns width in grid units
	virtual int calculateWidth() const = 0;
	// returns height in grid units
	virtual int calculateHeight() const = 0;
	// changes m_path based on m_width and m_height
	virtual void calculatePath() = 0;

	QPainterPath m_path;
};

#endif // GATEITEM_H
