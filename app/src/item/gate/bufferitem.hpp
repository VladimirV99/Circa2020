#ifndef BUFFERITEM_H
#define BUFFERITEM_H

#include "gateitem.hpp"

class BufferItem : public GateItem
{
public:
	BufferItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	BufferItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
};

#endif // BUFFERITEM_H
