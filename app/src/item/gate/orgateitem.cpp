#include "item/gate/orgateitem.hpp"

OrGateItem::OrGateItem(int posX, int posY, QGraphicsItem *parent)
	: OrGateItem(ComponentFactory::instance().makeOrGate(), posX, posY, parent)
{
}

OrGateItem::OrGateItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: GateItem(component, posX, posY, 0, 0, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::ORGATE)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getInputSize());
}

int OrGateItem::calculateHeight() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 0.75) / Scene::UNIT + 1;
}

int OrGateItem::calculateWidth() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
		return int(m_height * 0.75) / Scene::UNIT + 1;
	else
		return m_component->getInputSize() * 2;
}

void OrGateItem::calculatePath()
{
	if (m_orientation == Direction::RIGHT) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.arcTo(-(int)m_width / 4, 0, m_width / 2, m_height, 90, -180);
		m_path.lineTo(m_width / 2, m_height);
		m_path.arcTo(0, 0, m_width, m_height, -90, 180);
		m_path.lineTo(0, 0);
	} else if (m_orientation == Direction::DOWN) {
		m_path = QPainterPath(QPoint(0, 0));
		m_path.arcTo(0, -(int)m_height / 4, m_width, m_height / 2, -180, 180);
		m_path.lineTo(m_width, m_height / 2);
		m_path.arcTo(0, 0, m_width, m_height, 0, -180);
		m_path.lineTo(0, 0);
	} else if (m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(m_width, 0));
		m_path.arcTo(3 * (int)m_width / 4, 0, m_width / 2, m_height, 90, 180);
		m_path.lineTo(m_width / 2, m_height);
		m_path.arcTo(0, 0, m_width, m_height, -90, -180);
		m_path.lineTo(m_width, 0);
	} else {
		m_path = QPainterPath(QPoint(0, m_height));
		m_path.arcTo(0, 3 * (int)m_height / 4, m_width, m_height / 2, 180, -180);
		m_path.lineTo(m_width, m_height / 2);
		m_path.arcTo(0, 0, m_width, m_height, 0, 180);
		m_path.lineTo(0, m_height);
	}
}
