#include "item/gate/xorgateitem.hpp"

XorGateItem::XorGateItem(int posX, int posY, QGraphicsItem *parent)
	: XorGateItem(ComponentFactory::instance().makeXorGate(), posX, posY, parent)
{
}

XorGateItem::XorGateItem(Component *component, int posX, int posY, QGraphicsItem *parent)
	: GateItem(component, posX, posY, 0, 0, parent)
{
	if ((component == nullptr) || component->getType() != ComponentTypes::XORGATE)
		throw "Incompatible component for this item";
	m_isResizable = true;
	this->resizeInputs(m_component->getInputSize());
}

int XorGateItem::calculateHeight() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
		return m_component->getInputSize() * 2;
	else
		return int(calculateWidth() * Scene::UNIT * 0.75) / Scene::UNIT + 2;
}

int XorGateItem::calculateWidth() const
{
	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT)
		return int(m_height * 0.75) / Scene::UNIT + 2;
	else
		return m_component->getInputSize() * 2;
}

void XorGateItem::calculatePath()
{
}

void XorGateItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	QPen p = QPen();
	p.setWidth(2);
	painter->setPen(p);
	QBrush b = QBrush(Qt::white);
	QPainterPath arc(QPoint(0, 0));

	if (m_orientation == Direction::RIGHT || m_orientation == Direction::LEFT) {
		m_path = QPainterPath(QPoint(0, 0));
		arc.arcTo(-(int)m_width / 4, 0, m_width / 2, m_height, 90, -180);

		m_path.moveTo(m_width / 4, 0);
		m_path.arcTo(-m_width / 8 + 1, 0, m_width / 2, m_height, 90, -180);
		m_path.arcTo(0, 0, m_width, m_height, -90, 180);
		m_path.lineTo(m_width / 4, 0);
	} else {
		m_path = QPainterPath(QPoint(0, m_height));
		arc.moveTo(0, m_height);
		arc.arcTo(0, 3 * (int)m_height / 4, m_width, m_height / 2, 180, -180);

		m_path.moveTo(0, 3 * (int)m_height / 4);
		m_path.arcTo(0, 5 * (int)m_height / 8, m_width, m_height / 2, 180, -180);
		m_path.arcTo(0, 0, m_width, m_height, 0, 180);
		m_path.lineTo(0, 3 * (int)m_height / 4);
	}

	if (m_orientation == Direction::LEFT || m_orientation == Direction::DOWN) {
		QTransform t;
		t.rotate(180);
		t.translate(-m_width, -m_height);
		arc = t.map(arc);
		m_path = t.map(m_path);
	}

	painter->drawPath(arc);
	painter->setBrush(b);
	painter->drawPath(m_path);

	ComponentGraphicsItem::paint(painter, option, widget);
}
