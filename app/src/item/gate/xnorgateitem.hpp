#ifndef XNORGATEITEM_H
#define XNORGATEITEM_H

#include "gateitem.hpp"

class XnorGateItem : public GateItem
{
public:
	XnorGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	XnorGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
};

#endif // XNORGATEITEM_H
