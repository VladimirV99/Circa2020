#ifndef NORGATEITEMH_H
#define NORGATEITEMH_H

#include "gateitem.hpp"

class NorGateItem : public GateItem
{
public:
	NorGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	NorGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
};

#endif // NORGATEITEMH_H
