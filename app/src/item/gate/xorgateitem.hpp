#ifndef XORGATEITEM_H
#define XORGATEITEM_H

#include "gateitem.hpp"

class XorGateItem : public GateItem
{
public:
	XorGateItem(int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);
	XorGateItem(Component *component, int posX = 0, int posY = 0, QGraphicsItem *parent = nullptr);

protected:
	int calculateWidth() const override;
	int calculateHeight() const override;
	void calculatePath() override;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = nullptr) override;
};

#endif // XORGATEITEM_H
