#include "util/wiredrawutils.hpp"

#include "item/wiring/wireitem.hpp"

std::tuple<int, int, int, int> WireDrawUtils::getWireItemPosWH(
	const QPointF &start_point, const QPointF &scene_end_point, const Direction &direction)
{
	QPointF end_point;
	int width;
	int height;
	if (direction == Direction::RIGHT || direction == Direction::LEFT) {
		end_point = QPointF(scene_end_point.x(), start_point.y());
		height = 0;
		width = std::abs(round(start_point.x() / Scene::UNIT) - round(end_point.x() / Scene::UNIT));
	} else {
		end_point = QPointF(start_point.x(), scene_end_point.y());
		width = 0;
		height = std::abs(round(start_point.y() / Scene::UNIT) - round(end_point.y() / Scene::UNIT));
	}

	int posX = round(std::min(start_point.x(), end_point.x()) / Scene::UNIT);
	int posY = round(std::min(start_point.y(), end_point.y()) / Scene::UNIT);

	return {posX, posY, width, height};
}

QRectF WireDrawUtils::getPotentialWireItemOverlapBoundsSc(
	const QPointF &start_point, const QPointF &scene_end_point, const Direction &direction)
{
	auto inf = WireDrawUtils::getWireItemPosWH(start_point, scene_end_point, direction);
	int posX = std::get<0>(inf) * Scene::UNIT;
	int posY = std::get<1>(inf) * Scene::UNIT;
	int width = std::get<2>(inf) * Scene::UNIT;
	int height = std::get<3>(inf) * Scene::UNIT;

	return QRectF(
			   -Scene::POINT_SIZE / 2, -Scene::POINT_SIZE / 2, width + Scene::POINT_SIZE,
			   height + Scene::POINT_SIZE)
		.translated(posX, posY);
}

QRectF WireDrawUtils::getPotentialItemOverlapBoundsSc(int posx, int posy, int width, int height)
{
	return QRectF(0, 0, width, height).translated(posx, posy);
}

WireItem *WireDrawUtils::makeWireItem(
	const QPointF &start_point, const QPointF &scene_end_point, const Direction &direction, Wire *w)
{
	auto inf = WireDrawUtils::getWireItemPosWH(start_point, scene_end_point, direction);
	int posX = std::get<0>(inf);
	int posY = std::get<1>(inf);
	int width = std::get<2>(inf);
	int height = std::get<3>(inf);

	auto *wireItem = new WireItem(posX, posY, width, height, w, direction);
	return wireItem;
}

WireItem *WireDrawUtils::makeWireItem(const WireDrawInfo &info, const QPointF &scene_end_point, Wire *w)
{
	return WireDrawUtils::makeWireItem(info.start_point, scene_end_point, info.current_direction, w);
}

void WireDrawUtils::makeWireItemAndAdd(Scene *scene, Wire *w)
{
	auto *item = WireDrawUtils::makeWireItem(scene->m_drawing_wire_info, scene->m_endPoint, w);
	scene->addComponent(item);
}

void WireDrawUtils::singleSourceToWire(const WireDrawInfo &info, Wire *w)
{
	auto p_source_to_wire = Scene::directionLookup[static_cast<int>(info.source_directions[0])]
												  [static_cast<int>(info.current_direction)];
	unsigned to_wire = p_source_to_wire.second;

	//#ifdef WIRE_DRAW_LOGGING
	//    auto tag = "\t\t[SS2W] ";
	//    std::cerr << tag << "Connecting source "
	//              << info.source_components[0]->short_identify()
	//              << " directed "
	//              << info.source_directions[0]
	//              << " from idx "
	//              << info.source_indicies[0]
	//              << " to wire "
	//              << w->short_identify()
	//              << " directed "
	//              << info.current_direction
	//              << " to idx "
	//              << to_wire
	//              << std::endl;
	//#endif

	ConnectionManager::Instance().connect(
		info.source_components[0], info.source_indicies[0], w, to_wire, info.source_is_output[0]);
}

void WireDrawUtils::multiSourceToWire(const WireDrawInfo &info, Wire *w)
{
	//#ifdef WIRE_DRAW_LOGGING
	//    auto tag = "\t\t[MS2W] ";
	//    auto nl  = "\n\t\t";
	//    std::cerr << tag << " Connecting multisource to wire ";
	//#endif
	for (size_t i = 0; i < info.source_components.size(); i++) {
		auto *source_wire = info.source_components[i];
		auto source_direction = info.source_directions[i];
		auto p_source_to_wire =
			Scene::directionLookup[static_cast<int>(source_direction)][static_cast<int>(info.current_direction)];
		//#ifdef WIRE_DRAW_LOGGING
		//        std::cerr << nl
		//                  << " source " << source_wire->short_identify()
		//                  << " directed " << source_direction
		//                  << " from idx " << p_source_to_wire.first << nl
		//                  << " to wire " << w->short_identify()
		//                  << " directed " << info.current_direction
		//                  << " to idx " << p_source_to_wire.second;

		//#endif
		ConnectionManager::Instance().connect(source_wire, p_source_to_wire.first, w, p_source_to_wire.second);
		ConnectionManager::Instance().connect(w, p_source_to_wire.second, source_wire, p_source_to_wire.first);
	}
	//#ifdef WIRE_DRAW_LOGGING
	//    std::cerr << std::endl;
	//#endif
}

void WireDrawUtils::sourceToWire(const WireDrawInfo &info, Wire *w)
{
	if (info.source_components[0]->getType() == ComponentTypes::WIRE) {
		WireDrawUtils::multiSourceToWire(info, w);
	} else {
		WireDrawUtils::singleSourceToWire(info, w);
	}
}

void WireDrawUtils::wireToTarget(
	Wire *w, const Direction &wireDirection, const std::vector<Component *> &target_components,
	const std::vector<Direction> &target_directions, const std::vector<int> &target_indicies,
	const std::vector<bool> &target_is_output)
{
	if (target_components[0]->getType() == ComponentTypes::WIRE) {
		for (size_t i = 0; i < target_components.size(); i++) {
			auto *targetWire = target_components[i];
			auto targetDirection = target_directions[i];
			auto p = Scene::directionLookup[static_cast<int>(wireDirection)][static_cast<int>(targetDirection)];
			ConnectionManager::Instance().connect(w, p.first, targetWire, p.second);
			ConnectionManager::Instance().connect(targetWire, p.second, w, p.first);
			targetWire->setInput(p.second, targetWire->getOutput(p.second));
			targetWire->getInputConnections()[p.second].flush();
		}
	} else {
		auto *targetComponent = target_components[0];
		auto targetDirection = target_directions[0];
		auto targetIsOutput = target_is_output[0];
		auto targetIndex = target_indicies[0];
		auto p = Scene::directionLookup[static_cast<int>(wireDirection)][static_cast<int>(targetDirection)];
		ConnectionManager::Instance().connect(targetComponent, targetIndex, w, p.first, targetIsOutput);
	}
}
