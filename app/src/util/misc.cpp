#include "misc.hpp"

#include <QGraphicsView>

#include "item/componentgraphicsitem.hpp"
#include "item/wiring/wireitem.hpp"
#include "mainwindow.hpp"
#include "scene.hpp"

const ShouldConsiderPredicate MiscUtils::OVERLAP_CONSIDER_ALL = [](auto /*unused*/) { return true; };
const ShouldConsiderPredicate MiscUtils::OVERLAP_EXCLUDE_WIRES = [](ComponentGraphicsItem *it) {
	return it->m_component->getType() != ComponentTypes::WIRE;
};

ShouldConsiderPredicate MiscUtils::OVERLAP_EXCLUDE_THIS_ITEM(ComponentGraphicsItem *item)
{
	return [item](ComponentGraphicsItem *citem) { return item != citem; };
}

ShouldConsiderPredicate MiscUtils::OVERLAP_EXCLUDE_THIS_COMPONENT(Component *c)
{
	return [c](ComponentGraphicsItem *item) { return item->m_component != c; };
}

ShouldConsiderPredicate MiscUtils::OVERLAP_EXCLUDE_WIRES_CROSS(const Direction &dir)
{
	return [dir](ComponentGraphicsItem *item) {
		if (item->m_component->getType() != ComponentTypes::WIRE)
			return true;
		return DirectionUtil::sameOrientation(dir, static_cast<WireItem *>(item)->getDirection());
	};
}

bool MiscUtils::overlapsAnyItem(
	Scene *scene, const QRectF &bounds, const std::vector<ShouldConsiderPredicate> &preds)
{
	auto items = scene->items();
	ShouldConsiderPredicate finalPred;
	if (preds.empty()) {
		finalPred = MiscUtils::OVERLAP_CONSIDER_ALL;
	} else {
		finalPred = [preds](ComponentGraphicsItem *item) {
			return std::all_of(std::begin(preds), std::end(preds), [item](auto pred) { return pred(item); });
		};
	}

	return std::any_of(std::begin(items), std::end(items), [bounds, finalPred](QGraphicsItem *qitem) {
		auto *item = static_cast<ComponentGraphicsItem *>(qitem);
		bool ret = finalPred(item) && item->overlapBoundingRectSc().intersects(bounds);
		if (ret) {
			std::cerr << "\t\t\t --- overlap with " << item->m_component->short_identify() << std::endl;
		}
		return ret;
	});
}

bool MiscUtils::overlapsAnyItem(Scene *scene, const QRectF &bounds, const ShouldConsiderPredicate &pred)
{
	return MiscUtils::overlapsAnyItem(scene, bounds, std::vector<ShouldConsiderPredicate> {pred});
}

bool MiscUtils::overlapsAnyItem(Scene *scene, const QRectF &bounds)
{
	return MiscUtils::overlapsAnyItem(scene, bounds, MiscUtils::OVERLAP_CONSIDER_ALL);
}

bool MiscUtils::checkPotentialPosition(
	Scene *scene, ComponentGraphicsItem *item, int deltaX, int deltaY,
	const std::vector<ShouldConsiderPredicate> &preds)
{
	return !MiscUtils::overlapsAnyItem(scene, item->overlapBoundingRectSc().translated(deltaX, deltaY), preds);
}

bool MiscUtils::checkPotentialPosition(
	Scene *scene, ComponentGraphicsItem *item, int deltaX, int deltaY, const ShouldConsiderPredicate &pred)
{
	return !MiscUtils::overlapsAnyItem(scene, item->overlapBoundingRectSc().translated(deltaX, deltaY), {pred});
}

bool MiscUtils::checkPotentialPosition(Scene *scene, ComponentGraphicsItem *item, int deltaX, int deltaY)
{
	return checkPotentialPosition(scene, item, deltaX, deltaY, MiscUtils::OVERLAP_CONSIDER_ALL);
}
// --------------------------------------------------------------------------------------------------------------------------------------------------------------
std::optional<QPointF> MiscUtils::getGridDelta(const QPointF &previous, const QPointF &current)
{
	auto deltaPoint = current - previous;
	auto deltaX = deltaPoint.x();
	auto deltaY = deltaPoint.y();
	if (std::abs(deltaX) < Scene::UNIT && std::abs(deltaY) < Scene::UNIT)
		return {};

	int gridDeltaX;
	int gridDeltaY;
	if (std::abs(deltaX) >= std::abs(deltaY)) {
		gridDeltaX = ((deltaX > 0) ? 1 : -1) * Scene::UNIT;
		gridDeltaY = 0;
	} else {
		gridDeltaX = 0;
		gridDeltaY = ((deltaY > 0) ? 1 : -1) * Scene::UNIT;
	}

	return {QPointF(gridDeltaX, gridDeltaY)};
}

std::optional<QPointF> MiscUtils::getClosestAvailablePosition(Scene *scene, const QRectF &rect)
{
	return MiscUtils::getClosestAvailablePosition(scene, rect, {MiscUtils::OVERLAP_CONSIDER_ALL});
}

std::optional<QPointF> MiscUtils::getClosestAvailablePosition(
	Scene *scene, const QRectF &rect, const std::vector<ShouldConsiderPredicate> &preds)
{
	if (!MiscUtils::overlapsAnyItem(scene, rect, preds))
		return rect.topLeft();

	auto desiredWidth = rect.width();
	auto desiredHeight = rect.height();
	auto desiredX = rect.topLeft().x();
	auto desiredY = rect.topLeft().y();

	auto comp = [](const QPointF &a, const QPointF &b) {
		if (a.x() == b.x()) {
			return a.y() < b.y();
		}
		return a.x() < b.x();
	};

	std::vector<QPointF> solutions;
	std::set<QPointF, decltype(comp)> visited(comp);
	std::stack<QPointF> toCheck;
	toCheck.push(QPointF(desiredX, desiredY));

	auto visible_scene_rect = scene->getMainWindow()->getVisibleSceneArea();

	while (solutions.size() <= 100 && !toCheck.empty()) {
		auto candidate = toCheck.top();
		toCheck.pop();
		if (visited.find(candidate) != visited.end())
			continue;
		visited.insert(candidate);

		auto candidateRect = QRectF(candidate.x(), candidate.y(), desiredWidth, desiredHeight);
		if (scene->sceneRect().contains(candidate) && !MiscUtils::overlapsAnyItem(scene, candidateRect, preds))
			solutions.push_back(candidate);

		auto next1 = candidateRect.translated(Scene::UNIT, Scene::UNIT);
		auto next2 = candidateRect.translated(-Scene::UNIT, Scene::UNIT);
		auto next3 = candidateRect.translated(Scene::UNIT, -Scene::UNIT);
		auto next4 = candidateRect.translated(-Scene::UNIT, -Scene::UNIT);
		if (visible_scene_rect.contains(next1)) {
			toCheck.push(next1.topLeft());
		}
		if (visible_scene_rect.contains(next2)) {
			toCheck.push(next2.topLeft());
		}
		if (visible_scene_rect.contains(next3)) {
			toCheck.push(next3.topLeft());
		}
		if (visible_scene_rect.contains(next4)) {
			toCheck.push(next4.topLeft());
		}
	}
	if (solutions.empty())
		return {};

	auto desiredPoint = rect.topLeft();
	auto best_point = solutions[0];
	auto best_metric =
		std::pow(best_point.x() - desiredPoint.x(), 2) + std::pow(best_point.y() - desiredPoint.y(), 2);
	for (size_t i = 1; i < solutions.size(); i++) {
		auto curr_metric =
			std::pow(solutions[i].x() - desiredPoint.x(), 2) + std::pow(solutions[i].y() - desiredPoint.y(), 2);
		if (curr_metric < best_metric) {
			best_point = solutions[i];
			best_metric = curr_metric;
		}
	}
	return best_point;
}

QRectF MiscUtils::getGroupRect(const std::unordered_set<ComponentGraphicsItem *> &group)
{
	auto begin = std::begin(group);
	auto end = std::end(group);
	auto xmin = (*begin)->overlapBoundingRectSc().topLeft().x();
	auto xmax = (*begin)->overlapBoundingRectSc().bottomRight().x();
	auto ymin = (*begin)->overlapBoundingRectSc().topLeft().y();
	auto ymax = (*begin)->overlapBoundingRectSc().bottomLeft().y();

	for (auto it = begin; it != end; it++) {
		auto *p = *it;
		xmin = std::min(xmin, p->overlapBoundingRectSc().topLeft().x());
		xmax = std::max(xmax, p->overlapBoundingRectSc().topRight().x());
		ymin = std::min(ymin, p->overlapBoundingRectSc().topLeft().y());
		ymax = std::max(ymax, p->overlapBoundingRectSc().bottomLeft().y());
	}

	return QRectF(QPointF(xmin, ymin), QPointF(xmax, ymax));
}

QPointF MiscUtils::getAnchor(const std::unordered_set<ComponentGraphicsItem *> &items)
{
	auto begin = std::begin(items);
	auto end = std::end(items);

	QPointF minPoint = (*begin)->scenePos();
	for (auto it = begin; it != end; it++) {
		auto *currItem = *it;
		minPoint.setX(std::min(minPoint.x(), currItem->scenePos().x()));
		minPoint.setY(std::min(minPoint.y(), currItem->scenePos().y()));
	}
	return minPoint;
}
