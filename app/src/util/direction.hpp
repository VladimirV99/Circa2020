#ifndef DIRECTION_H
#define DIRECTION_H

#include <iostream>

enum class Direction { RIGHT = 0, LEFT = 1, UP = 2, DOWN = 3 };

class DirectionUtil
{
public:
	static bool sameOrientation(const Direction &d1, const Direction &d2);
	static Direction reverse(const Direction &d);
	static bool horizontal(const Direction &);
	static bool vertical(const Direction &);

	static int toInt(Direction direction);
	static Direction fromInt(int d);
	static void serialize(std::ostream &os, Direction direction);
	static Direction deserialize(std::istream &is);
};

inline std::ostream &operator<<(std::ostream &out, const Direction &dir)
{
	if (dir == Direction::LEFT)
		out << "LEFT";
	else if (dir == Direction::RIGHT)
		out << "RIGHT";
	else if (dir == Direction::UP)
		out << "UP";
	else
		out << "DOWN";
	return out;
}

#endif // DIRECTION_H
