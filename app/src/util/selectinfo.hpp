#ifndef SELECTINFO_H
#define SELECTINFO_H

#include <unordered_set>

class ComponentGraphicsItem;

struct SelectInfo {
	std::unordered_set<ComponentGraphicsItem *> selection;
	bool holding_ctrl;
	bool outside_connections_terminated;
	bool in_manual_selection;
	bool selection_is_moving;
	bool handled;
};

#endif // SELECTINFO_H
