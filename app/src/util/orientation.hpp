#ifndef ORIENTATION_H
#define ORIENTATION_H

#include <iostream>

// Orientation represents direction of lightbulbs
enum class Orientation { NORTH = 0, EAST = 1, SOUTH = 2, WEST = 1 };

#endif // ORIENTATION_H
