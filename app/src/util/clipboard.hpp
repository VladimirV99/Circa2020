#ifndef CLIPBOARD_H
#define CLIPBOARD_H

#include <unordered_set>

#include <QRectF>

class ComponentGraphicsItem;

struct ClipBoard {
	std::unordered_set<ComponentGraphicsItem *> items;
	bool holding_ctrl;
	QRectF selectionRect;
	QPointF anchor;
};

#endif // CLIPBOARD_H
