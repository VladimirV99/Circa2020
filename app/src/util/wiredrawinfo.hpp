#ifndef WIREDRAWINFO_H
#define WIREDRAWINFO_H

#include <vector>

#include <QPointF>

#include "direction.hpp"

class ComponentGraphicsItem;
class Component;

struct WireDrawInfo {
	//    WireDrawState            state = WireDrawState::NOTHING;
	bool drawing = false;
	std::vector<Component *> source_components = {};
	std::vector<bool> source_is_output = {};
	std::vector<Direction> source_directions = {};
	std::vector<int> source_indicies = {};
	Direction current_direction = Direction::RIGHT;
	QPointF start_point = QPointF(0, 0);
};

struct IndirectClickReport {
	ComponentGraphicsItem *clickedItem = nullptr;
	int cpIndex = 0;
	bool cpIsOutput = false;
	Direction cpDirection = Direction::RIGHT;
	IndirectClickReport(ComponentGraphicsItem *clickedItem, unsigned cpIndex, bool cpIsOutput, Direction direction)
		: clickedItem(clickedItem), cpIndex(cpIndex), cpIsOutput(cpIsOutput), cpDirection(direction)
	{
	}
};

#endif // WIREDRAWINFO_H
