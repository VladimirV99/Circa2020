#include "colorutil.hpp"

ColorUtil::ColorUtil() = default;

const QColor ColorUtil::UNDEFINED = Qt::blue;
const QColor ColorUtil::ERROR = Qt::red;
const QColor ColorUtil::FALSE = Qt::darkGreen;
const QColor ColorUtil::TRUE = Qt::green;

QColor ColorUtil::forState(Signal::State state)
{
	switch (state) {
	case Signal::State::UNDEFINED:
		return UNDEFINED;
	case Signal::State::ERROR:
		return ERROR;
	case Signal::State::FALSE:
		return FALSE;
	case Signal::State::TRUE:
		return TRUE;
	}
	return Qt::white;
}
