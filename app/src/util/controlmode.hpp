#ifndef CONTROLMODE_H
#define CONTROLMODE_H

#include <iostream>

enum class ControlMode {
	DESIGN,	   // Creating components, moving them around and drawing wires
	SELECTION, // Selecting one or more components, moving selections, copying selections, etc
	PINS	   // Flipping pins
};

inline std::ostream &operator<<(std::ostream &out, const ControlMode &cm)
{
	if (cm == ControlMode::DESIGN)
		out << "Design mode";
	else if (cm == ControlMode::SELECTION)
		out << "Selection mode";
	else if (cm == ControlMode::PINS)
		out << "Pin flipping mode";
	return out;
}

#endif // CONTROLMODE_H
